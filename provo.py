#!/usr/bin/python3

from flask import Flask
from flask_restful import reqparse, abort, Api, Resource

import psycopg2
from psycopg2.extras import RealDictCursor
import sys
import copy
import re
import datetime
import inspect
import importlib

importlib.import_module('db')
app = Flask(__name__)
api = Api(app)

parser = reqparse.RequestParser()

# Fields for top level objects

fields = { 
    'agent' : [
        'name'
    ],
    'activity': [
        'name',
        'ended_at_time',
        'started_at_time'
    ],
    'entity': [
        'name',
        'generated_at_time',
        'invalidated_at_time',
        'has_value'
    ],
    # "influence" is an object property, but has a domain of owl:Thing, i.e. it
    # can point at anything, including all entities in this database, therefore
    # this becomes a field, even though the relations entity, agent and
    # activity are sub-object properties.
    'influence': [
        'name',
        'influencer'
    ],
    'instantaneous_event': [
        'name',
        'at_time'
    ],
    'location': [
        'name'
    ],
    'role': [
        'name'
    ]
}

# Fields for children objects

# N.B. The assigment does not copy, but passes a reference, so beware! 

fields['organization'] = fields['agent']
fields['person'] = fields['agent']
fields['software_agent'] = fields['agent']

fields['bundle'] = fields['entity']
fields['collection'] = fields['entity']
fields['empty_collection'] = fields['collection']
fields['plan'] = fields['entity']

fields['activity_influence'] = fields['influence']
fields['agent_influence'] = fields['influence']
fields['entity_influence'] = fields['influence']

fields['ends'] = list( 
        set(fields['instantaneous_event']) | 
        set(fields['entity_influence'])) + ['ended_at_time']

fields['generation'] = list(
        set(fields['instantaneous_event']) |
        set(fields['activity_influence'])) + ['generated_at_time']

fields['invalidation'] = list(
        set(fields['instantaneous_event']) | 
        set(fields['activity_influence'])) + ['invalidated_at_time']

fields['start'] = list(
        set(fields['instantaneous_event']) | 
        set(fields['entity_influence'])) + ['started_at_time']
 
fields['usage'] = list(
        set(fields['instantaneous_event']) | 
        set(fields['entity_influence']))
 
fields['communication'] = fields['activity_influence']

fields['association'] = fields['agent_influence']

fields['attribution'] = fields['agent_influence']

fields['delegation'] = fields['agent_influence']

fields['derivation'] = fields['entity_influence']

fields['primary_source'] = fields['derivation']

fields['quotation'] = fields['derivation']

fields['revision'] = fields['derivation']

# Subclass definition

has_subclass = {
        'agent': ['organization','person','software_agent'],
        'entity': ['bundle','collection','plan'],
        'collection': ['empty_collection'],
        'influence': ['activity_influence','agent_influence','entity_influence'],
        'activity_influence': ['communication','generation','invalidation'],
        'agent_influence': ['association','attribution','delegation'],
        'entity_influence': ['derivation','ends','start','usage'],
        'derivation': ['primary_source','quotation','revision'],
        'instantaneous_event': ['ends','generation','invalidation','start','usage'],
        }

# Foreign keys for top/parent objects

foreign_keys = {
    'activity': [
        'at_location',
        'generated',
        'invalidated',
        'qualified_association',
        'qualified_communication',
        'qualified_end',
        'qualified_influence',
        'qualified_start',
        'qualified_usage',
        'used',
        'was_associated_with',
        'was_ended_by',
        'was_generated_by',
        'was_influenced_by',
        'was_informed_by',
        'was_invalidated_by',
        'was_started_by'
    ],
    'agent': [
        'at_location',
        'qualified_delegation',
        'qualified_influence',
        'acted_on_behalf_of',
        'was_influenced_by'
    ],
    'entity': [
        'alternate_of',
        'at_location',
        'had_primary_source',
        'qualified_attribution',
        'qualified_derivation',
        'qualified_generation',
        'qualified_influence',
        'qualified_invalidation',
        'qualified_primary_source',
        'qualified_quotation',
        'qualified_revision',
        'specialization_of',
        'was_attributed_to',
        'was_derived_from',
        'was_generated_by',
        'was_influenced_by',
        'was_invalidated_by',
        'was_quoted_from',
        'was_revision_of'
    ],
    'influence': [
        'had_activity',
        'had_role',
    ],
    'instantaneous_event': [
        'at_location',
        'had_role',
    ]
}

# Foreign keys for children objects

foreign_keys['organization'] = foreign_keys['agent']
foreign_keys['person'] = foreign_keys['agent']
foreign_keys['software_agent'] = foreign_keys['agent']

foreign_keys['bundle'] = foreign_keys['entity']
foreign_keys['collection'] = foreign_keys['entity'] + ['had_member']
foreign_keys['empty_collection'] = foreign_keys['collection']
foreign_keys['plan'] = foreign_keys['entity']

foreign_keys['activity_influence'] = (
    foreign_keys['influence'] +
    ['activity'])
foreign_keys['agent_influence'] = (
    foreign_keys['influence'] +
    ['agent'])
foreign_keys['entity_influence'] = (
    foreign_keys['influence'] +
    ['entity'])

foreign_keys['communication'] = (
    foreign_keys['activity_influence'])
foreign_keys['generation'] = list(
    set(foreign_keys['instantaneous_event']) |
    set(foreign_keys['activity_influence'])) 
foreign_keys['invalidation'] = list(
    set(foreign_keys['instantaneous_event']) |
    set(foreign_keys['activity_influence'])) 

foreign_keys['association'] = (
    foreign_keys['agent_influence'] +
    ['had_plan', 'had_role'])
foreign_keys['attribution'] = (
    foreign_keys['agent_influence'])
foreign_keys['delegation'] = (
    foreign_keys['agent_influence'] +
    ['had_activity'])

foreign_keys['derivation'] = (
    foreign_keys['entity_influence'] +
    ['had_activity', 'had_generation', 'had_usage'])
foreign_keys['primary_source'] = (
    foreign_keys['derivation'])
foreign_keys['quotation'] = (
    foreign_keys['derivation'])
foreign_keys['revision'] = (
    foreign_keys['derivation'])

foreign_keys['ends'] = list(
    set(foreign_keys['instantaneous_event']) |
    set(foreign_keys['entity_influence'])) + ['had_activity']  
foreign_keys['start'] = list(
    set(foreign_keys['instantaneous_event']) |
    set(foreign_keys['entity_influence'])) + ['had_activity']  
foreign_keys['usage'] = list(
    set(foreign_keys['instantaneous_event']) |
    set(foreign_keys['entity_influence']))

valid_fields = set()
valid_foreign_keys = set()
for table in fields.keys():
    valid_fields = valid_fields | set(fields[table])
valid_arguments = valid_fields.copy()
for table in foreign_keys.keys():
    valid_foreign_keys = valid_foreign_keys | set(foreign_keys[table])
    valid_arguments = valid_arguments | set(foreign_keys[table])

for arg in valid_arguments:
    parser.add_argument(arg, action='append')

# Top-level/parent domains for object properties

sub_properties_of = {
    'specialization_of' : ['alternate_of'],
    'generated': ['influenced'],
    'invalidated': ['influenced'],
    'qualified_association': ['qualified_influence'],
    'qualified_attribution': ['qualified_influence'],
    'qualified_communication': ['qualified_influence'],
    'qualified_delegation': ['qualified_influence'],
    'qualified_derivation': ['qualified_influence'],
    'qualified_end': ['qualified_influence'],
    'qualified_generation': ['qualified_influence'],
    'qualified_invalidation': ['qualified_influence'],
    'qualified_primary_source': ['qualified_influence'],
    'qualified_quotation': ['qualified_influence'],
    'qualified_revision': ['qualified_influence'],
    'qualified_start': ['qualified_influence'],
    'acted_on_behalf_of': ['was_influenced_by'],
    'had_member': ['was_influenced_by'],
    'used': ['was_influenced_by'],
    'was_associated_with': ['was_influenced_by'],
    'was_attributed_to': ['was_influenced_by'],
    'was_derived_from': ['was_influenced_by'],
    'was_ended_by': ['was_influenced_by'],
    'was_generated_by': ['was_influenced_by'],
    'was_informed_by': ['was_influenced_by'],
    'was_invalidated_by': ['was_influenced_by'],
    'had_primary_source': ['was_derived_from'],
    'was_quoted_from': ['was_derived_from'],
    'was_revision_of': ['was_derived_from']
}

# A note for yourself, because you forgot this. The reason for the list of
# lists on domains is that the lists represent intersections, whereas the
# contents of the lists are unions.

all_domains = {
    'alternate_of': [ ['entity'] ],
    'at_location': [ ['activity', 'agent', 'entity', 'instantaneous_event' ] ],
    'had_activity': [ ['influence'], ['delegation', 'derivation', 'ends', 'start'] ],
    'had_generation': [ ['derivation'] ],
    'had_plan': [ ['association'] ],
    'had_role': [ ['influence'], ['association', 'instantaneous_event'] ],
    'had_usage': [ ['derivation'] ],
    'influenced': [],
    'qualified_influence': [ ['activity','agent','entity'] ],
    'was_influenced_by': [ ['activity','agent','entity'] ],
    'generated': [ ['activity'] ],
    'invalidated': [ ['activity'] ],
    'activity': [ ['activity_influence'] ],
    'agent': [ ['agent_influence'] ],
    'entity': [ ['entity_influence'] ],
    'qualified_association': [ ['activity'] ],
    'qualified_attribution': [ ['entity'] ],
    'qualified_communication': [ ['activity'] ],
    'qualified_delegation': [ ['agent'] ],
    'qualified_derivation': [ ['entity'] ],
    'qualified_end': [ ['activity'] ],
    'qualified_generation': [ ['entity'] ],
    'qualified_invalidation': [ ['entity'] ],
    'qualified_primary_source': [ ['entity'] ],
    'qualified_quotation': [ ['entity'] ],
    'qualified_revision': [ ['entity'] ],
    'qualified_start': [ ['activity'] ],
    'qualified_usage': [ ['activity'] ],
    'acted_on_behalf_of': [ ['agent'] ],
    'had_member': [ ['collection'] ],
    'used': [ ['activity'] ],
    'was_associated_with': [ ['activity'] ],
    'was_attributed_to': [ ['entity'] ],
    'was_derived_from': [ ['entity'] ],
    'was_ended_by': [ ['activity'] ],
    'was_generated_by': [ ['entity'] ],
    'was_informed_by': [ ['activity'] ],
    'was_invalidated_by': [ ['entity'] ],
    'was_started_by': [ ['activity'] ],
    'had_primary_source': [ ['entity'] ],
    'was_quoted_from': [ ['entity'] ],
    'was_revision_of': [ ['entity'] ]
}

all_codomains = {
    'alternate_of': [ ['entity'] ],
    'at_location': [ ['location'] ],
    'had_activity': [ ['activity'] ],
    'had_generation': [ ['generation'] ],
    'had_plan': [ ['plan'] ],
    'had_role': [ ['role'] ],
    'had_usage': [ ['usage'] ],
    'influenced': [],
    'qualified_influence': [ ['influence'] ],
    'was_influenced_by': [ ['activity', 'agent', 'entity'] ],
    'specialization_of': [ ['entity'] ],
    'generated': [ ['entity'] ],
    'invalidated': [ ['entity'] ],
    'activity': [ ['activity'] ],
    'agent': [ ['agent'] ],
    'entity': [ ['entity'] ],
    'qualified_association': [ ['association'] ],
    'qualified_attribution': [ ['attribution'] ],
    'qualified_communication': [ ['communication'] ],
    'qualified_delegation': [ ['delegation'] ],
    'qualified_derivation': [ ['derivation'] ],
    'qualified_end': [ ['ends'] ],
    'qualified_generation': [ ['generation'] ],
    'qualified_invalidation': [ ['invalidation'] ],
    'qualified_primary_source': [ ['primary_source'] ],
    'qualified_quotation': [ ['quotation'] ],
    'qualified_revision': [ ['revision'] ],
    'qualified_start': [ ['start'] ],
    'qualified_usage': [ ['usage'] ],
    'acted_on_behalf_of': [ ['agent'] ],
    'had_member': [ ['entity'] ],
    'used': [ ['entity'] ],
    'was_associated_with': [ ['agent'] ],
    'was_attributed_to': [ ['agent'] ],
    'was_derived_from': [ ['entity'] ],
    'was_ended_by': [ ['entity'] ],
    'was_generated_by': [ ['activity'] ],
    'was_informed_by': [ ['activity'] ],
    'was_invalidated_by': [ ['activity'] ],
    'was_started_by': [ ['entity'] ],
    'had_primary_source': [ ['entity'] ],
    'was_quoted_from': [ ['entity'] ],
    'was_revision_of': []
    }


antonyms = {
    "influenced": "was_influenced_by",
    "was_influenced_by": "influenced",
    "generated": "was_generated_by",
    "was_generated_by": "generated",
    "invalidated": "was_invalidated_by",
    "was_invalidated_by": "invalidated"
    }

def print_tables():

    # Print out all the fixed metadata stuff...

    # Now print out the bog-standard tables...

    for table in fields.keys():
        print('CREATE TABLE IF NOT EXISTS ' + table + ' (')
        lines = ""

        for field in fields[table]:
            if field == 'name':
                lines = lines + '\t' + field.upper() + ' TEXT PRIMARY KEY,\n'
            elif field == 'has_value' or field == 'influencer':
                lines = lines + '\t' + field.upper() + ' TEXT,\n'
            else:
                lines = lines + '\t' + field.upper() + ' TIMESTAMP,\n'

        lines = lines[:-2]
        print(lines + '\n);')

    # Now print out the reifications. This is all the foreign keys

    for foreign_key in valid_foreign_keys:
        domains, codomains = io(foreign_key)
        for domain in set(domains):
            for codomain in set(codomains):
                domain_column_name = domain
                codomain_column_name = codomain 
                #if codomain== 'owl:Thing':
                #   codomain= 'everything'
                print('CREATE TABLE IF NOT EXISTS ' + 
                        reification(domain,codomain,foreign_key) + ' (')
                print('\tSOURCE TEXT,')
                if codomain == 'everything':
                    print('\tTARGET TEXT')
                else:
                    print('\tTARGET TEXT,')
                    print('\tPRIMARY KEY(SOURCE, TARGET),')
                    print('\tFOREIGN KEY(SOURCE) REFERENCES ' +
                            domain + 
                            '(NAME),')
                    print('\tFOREIGN KEY(TARGET) REFERENCES ' +
                            codomain + 
                            '(NAME)')
                print(');\n\n')


def io(foreign_key):

    domains = []
    codomains = []

    if foreign_key in all_domains.keys():
        for domain_list in all_domains[foreign_key]:
            for domain in domain_list:
                domains = domains + subclasses(domain)
            for codomain_list in all_codomains[foreign_key]:
                for codomain in codomain_list:
                    codomains = codomains + subclasses(codomain)

    if foreign_key in sub_properties_of:
        for super_property in sub_properties_of[foreign_key]:
            super_io = io(super_property) 
            domains = domains + super_io[0]
            codomains  = codomains + super_io[1]
    return (list(set(domains)),list(set(codomains)))

def subclasses(class_type):
    my_subclasses = [class_type]; 
    if class_type in has_subclass.keys():
        for a_subclass in has_subclass[class_type]:
            my_subclasses = my_subclasses + subclasses(a_subclass)
    return my_subclasses

def print_drop_tables():
    for table in fields.keys():
        print('DROP TABLE IF EXISTS ' + table + ' CASCADE;')
    for foreign_key in valid_foreign_keys:
        domains,codomains = io(foreign_key)
        for domain in set(domains):
            for codomain in set(codomains):
                domain_column_name = domain
                codomain_column_name = codomain
                #if codomain == 'owl:Thing':
                #    codomain = 'everything'
                print('DROP TABLE IF EXISTS ' +  
                        reification(domain,codomain,foreign_key) + ';')
    
def get_connection():
    try:
        sql = ("dbname='" + provo + "' user='" + user + "' " +
               "host='" + host + " password='" + password + "''")
        conn = psycopg2.connect(sql)
    except Exception as ex:
        abort(404, message=format_exception(ex,sql))
    return conn

def generic_get(self, key, table):
    conn = get_connection();
    cur = conn.cursor(cursor_factory=RealDictCursor)

    sql = ('select * from ' + 
            table + " where name = '" + 
            key.replace("'","''") + "'")
    print(sql)
    try:
        cur.execute(sql)
    except Exception as ex:
        abort(4204, message=format_exception(ex,sql))

    row = cur.fetchone()

    if row == None:
        return {}, 200;

    if table in foreign_keys:
        for foreign_key in foreign_keys[table]:
            domains,codomains = io(foreign_key)
            for codomain in codomains:
                #if codomain == 'owl:Thing':
                #    continue
                sql = ('select source,target from ' + 
                   reification(table, codomain, foreign_key)  + 
                   " where source = '" + 
                   row['name'].replace("'","''") + "'")
                print(sql)
                try:
                    cur.execute(sql)
                except Exception as ex:
                    abort(404, message=format_exception(ex,sql))
                for foreign_row in cur.fetchall():
                    if not foreign_key in row.keys():
                        row[foreign_key] = [ codomain + '(' + foreign_row['target'] + ')' ]
                    else:
                        row[foreign_key].append(codomain + '(' + foreign_row['target'] + ')')

    cur.close()
    return wrangled(row), 200

def wrangled(row_or_rows):

    # Apparently flask uses a json which cannot serialize datetimes (!!!!).
    # You can get around this using the quick and dirty

    # return json.dumps(row_or_rows, sort_keys=True, default=str)

    # but this effs up the returned json by turning it into a string.

    # So we are going to have to do this the (yucky and) old fashioned way.

    if isinstance(row_or_rows, dict):
        for column in row_or_rows.keys():
            if type(row_or_rows[column]) is datetime.datetime:
                new = row_or_rows[column].strftime("%Y-%m-%d %H:%M:%S")
                row_or_rows[column] = new
        return row_or_rows

    modified_rows = []
    for row in row_or_rows:
        print(str(row))
        modified_rows.append(wrangled(row))
    return modified_rows

def generic_delete(self, key, table):
    conn = get_connection();
    cur = conn.cursor(cursor_factory=RealDictCursor)
    
    # First we have to get rkey of all instances of this as range/codomain in
    # other tables.


    sql = ('select * from ' + table + 
            " where name = '" + key.replace("'","''") + 
            "'")
    print(sql)
    try:
        cur.execute(sql)
    except Exception as ex:
        abort(404, message=format_exception(ex,sql))

    row = cur.fetchone()

    if row == None:
        return '', 204

    # All the relationships for which this key is in the domain

    if table in foreign_keys.keys():
        for foreign_key in foreign_keys[table]:
            domains,codomains = io(foreign_key)
            if table in domains:
                for codomain in codomains:
                    sql = ('delete from ' + 
                        reification(table, codomain, foreign_key)
                        + ' where source = ' + "'" + key.replace("'","''") + "'")
                    print(sql)
                    try:
                        cur.execute(sql)
                    except Exception as ex:
                        abort(404, message=format_exception(ex,sql))

   
        
    # Now do the relationships with codomains with this key

    for other_table in foreign_keys.keys():
        for foreign_key in foreign_keys[other_table]:
            domains,codomains = io(foreign_key)
            if table in codomains:
                for domain in domains:
                    sql = ('delete from ' + 
                        reification(domain, table, foreign_key) + 
                        ' where target = ' + "'" + 
                        key.replace("'","''") + "'")
                    print(sql)
                    try:
                        cur.execute(sql)
                    except Exception as ex:
                        abort(404, message=format_exception(ex,sql))


    sql = ('delete from ' + table + 
        " where name = '" + key.replace("'","''") + "'")
    print(sql)
    try:
        cur.execute(sql)
    except Exception as ex:
        abort(404, message=format_exception(ex,sql))

    conn.commit()
    cur.close()

    return '', 204

def generic_put(self, key, table):
    args = parser.parse_args()

    generic_delete(self, key, table);

    conn = get_connection();
    cur = conn.cursor(cursor_factory=RealDictCursor)

    sql = ('select name from ' + table + 
            " where name = '" + key.replace("'","''") + 
            "'")
    print(sql)
    try:
        cur.execute(sql)
    except Exception as ex:
        abort(404, message=format_exception(ex,sql))

    rows = cur.fetchall()

    if len(rows) == 0:
        sql_col_names = 'name,'
        sql_col_values = "'" + key.replace("'","''") + "',"
        for arg  in args.keys():
            if args[arg] != None and arg in fields[table]:
                sql_col_names = sql_col_names + arg + ','
                sql_col_values = (sql_col_values + "'" + 
                args[arg][0].replace("'","''") + "',")
        sql = ('insert into ' + table + ' (' + sql_col_names[:-1] +  ') ' +
                "values (" + sql_col_values[:-1] + ')') 
    else:
        sql = 'update ' + table + " set name='" + key + "',"
        for arg  in args.keys():
            if args[arg] != None and arg in fields[table]:
                sql = (sql + arg + "='" + 
                args[arg][0].replace("'","''") + "',")	
        sql = (sql[:-1] + " where name = '" + 
                key.replace("'","''") + "'")	

    print(sql)
    try:
        cur.execute(sql)
    except Exception as ex:
        abort(404, message=format_exception(ex,sql))

    conn.commit()

    for arg in args.keys():
        if args[arg] != None and arg in foreign_keys[table]:
            domains,codomains = io(arg)
            if not arg in foreign_keys[table]:
                abort(404, message=('generic_put: trying to use a ' + 
                    'relationship, ' + arg + ' invalid for ' + table))
            if not table in domains:
                abort(404, message=('generic_put: trying to use a ' + 
                    'relationship, ' + arg + ' with no domain in ' + table))

            # So there might be more than one value for codomain specified and
            # moreover the the codomain might be ambiguous, i.e it might be
            # more than one target table. If it is then the table name should
            # be specified, remembering that there might be a superclass as
            # well, so:

            for row in args[arg]:
                target = ""
                m = re.search('^(.+)\((.+)\)', row)
                if m:
                    target = m.group(1)
                    row = m.group(2)
                    
                if len(set(codomains)) == 1 and target == "":
                    target = codomains[0]

                if target == "":
                    abort(404, message='generic_put: trying to use a relati' + 
                        'onship ' + arg + ' for ' + table + ' with ambiguous' + 
                        'targets, having not specified a target table in the' + 
                        ' argument' + row + ' must be one of ' +
                         str(set(codomains)))
                some_table = reification(table, target, arg)
                sql = ('select source,target from ' + some_table +
                    " where  source = '" + key.replace("'","''") + 
                    "' and target = '" + row.replace("'","''") + 
                    "'")
                print(sql)
                try:
                    cur.execute(sql)
                except Exception as ex:
                    abort(404, message=format_exception(ex,sql))

                rows = cur.fetchall()

                # If the record is already there, there is nothing to change,
                # because this only consists of two fields: source and target.

                if len(rows) == 0:
                    sql = ('insert into ' + 
                            some_table + ' (source,target) ' + 
                            " values ('" + key.replace("'","''")  + "','" + 
                            row.replace("'","''") + "')")
                    print(sql)
                    try:
                        cur.execute(sql)
                    except Exception as ex:
                        abort(404, message=format_exception(ex,sql))

                    conn.commit()

    cur.close()

    return key, 201

def generic_get_all(self,table):

    conn = get_connection();
    cur = conn.cursor(cursor_factory=RealDictCursor)

    sql = ('select * from ' + table)
    print(sql)
    try:
        cur.execute(sql)
    except Exception as ex:
        abort(404, message=format_exception(ex,sql))

    rows = cur.fetchall()

    if table in foreign_keys.keys():
        for foreign_key in foreign_keys[table]:
            domains,codomains = io(foreign_key)
            for codomain in codomains:
                for row in rows: 
                    sql = ('select source,target from ' + 
                            reification(table, codomain, foreign_key) + 
                            " where source = '" + 
                            row["name"].replace("'","''") + "'")
                    print(sql)
                    try:
                        cur.execute(sql)
                    except Exception as ex:
                        abort(404, message=format_exception(ex,sql))
                    for foreign_row in cur.fetchall():
                        if not foreign_key in row.keys():
                            row[foreign_key] = [ foreign_row["target"] ]
                        else:
                            row[foreign_key].append(foreign_row["target"])

    cur.close()

    return wrangled(rows), 200

def format_exception(ex, sql):

    lineno = inspect.currentframe().f_back.f_lineno
    template = (
            "line: {0}\n" +
            "SQL: {1}\n" +
            "An exception of type {2} occurred.\n" +
            "Arguments:\n{3!r}")
    return template.format(lineno, sql, type(ex).__name__, ex.args)

def generic_post_all(self,table):
    args = parser.parse_args()

    conn = get_connection();
    cur = conn.cursor(cursor_factory=RealDictCursor)

    generic_delete_all(self, table)

    if args['name'] == None:
        abort(404, 
            message=('generic_post_all: ' + table + ' must at least supply ' +
            '"name" parameter'))

    for arg_name  in args.keys():
        if args[arg_name] != None and len(args['name']) != len(args[arg_name]):
            abort(404, 
                message=('generic_post_all: ' + table + ' number of ' +
                'parameters in a type vary from name to ' + arg_name))

    for i in range(len(args['name'])):
        key = args['name'][i]
        sql_col_names = ''
        sql_col_values = ''
        for arg_name in args.keys():
            if args[arg_name] == None or not arg_name in fields[table]:
                continue
            sql_col_names = sql_col_names + arg_name + ','
            if args[arg_name][i] == '':
                sql_col_values = (sql_col_values + 'null,')
            else:
                print(args[arg_name][i] + "'," + str(i))
                sql_col_values = (sql_col_values + "'" + 
                    args[arg_name][i].replace("'","''") + "',")
        sql = ('insert into ' + table + ' (' + sql_col_names[:-1] +  ') ' +
            "values (" + sql_col_values[:-1] + ');') 
        print(sql)
        try:
            cur.execute(sql)
            conn.commit()
        except Exception as ex:
            abort(404, message=format_exception(ex,sql))

        for arg in args.keys():
            if (args[arg] == None or 
            not arg in foreign_keys[table] or
            args[arg][i] == ''):
                continue

            domains, codomains = io(arg)
            if not arg in foreign_keys[table]:
                abort(404, message=('generic_put_all: trying to use a ' + 
                    'relationship, ' + arg + ' invalid for ' + table))
            if not table in domains:
                abort(404, message=('generic_put_all: trying to use a ' + 
                    'relationship, ' + arg + ' with no domain in ' + table))

            field = args[arg][i]

            m = re.search('^(.+)\((.+)\)', field)
            if m:
                target = m.group(1)
                field = m.group(2)
                
            if len(set(codomains)) == 1 and target == '':
                target = codomains[0]

            if target == '':
                abort(404, message='generic_put_all: trying to use a ' + 
                    'relationship, ' + arg + ' for ' + table + 
                    ' with ambiguous targets, having not specified a ' + 
                    'target table in the argument value  "' + field + 
                    '", must be one of ' + str(set(codomains)) + ' on ' +
                    'key ' + args['name'][i] + '.')

            sql = ('insert into ' + reification(table, target, arg) + 
                ' (source,target) ' + " values ('" + key .replace("'","''") + "','" + field + "')")
            print(sql)
            try:
                cur.execute(sql)
            except Exception as ex:
                abort(404, message=format_exception(ex,sql))

            conn.commit()

    cur.close()

    return args['name'], 201

def generic_delete_all(self, table):
    conn = get_connection();
    cur = conn.cursor(cursor_factory=RealDictCursor)

    sql = ('select * from ' + table)
    print(sql)
    try:
        cur.execute(sql)
    except Exception as ex:
        abort(404, message=format_exception(ex,sql))

    rows = cur.fetchall()

    cur.close()

    if len(rows) < 1:
        return '', 204

    for row in rows:
        print(str(row['name']))
        generic_delete(self, row['name'], table)

    return '', 204

class Activity(Resource):
    def get(self, activity_key):
        return generic_get(self, activity_key, 'activity')
    def delete(self, activity_key):
        return generic_delete(self, activity_key, 'activity')
    def put(self, activity_key):
        return generic_put(self, activity_key, 'activity')

class Activity_List(Resource):
    def get(self):
        return generic_get_all(self, 'activity')
    def post(self):
        return generic_post_all(self, 'activity')
    def delete(self):
        return generic_delete_all(self, 'activity')

class Agent(Resource):
    def get(self, agent_key):
        return generic_get(self, agent_key, 'agent')
    def delete(self, agent_key):
        return generic_delete(self, agent_key, 'agent')
    def put(self, agent_key):
        return generic_put(self, agent_key, 'agent')

class Agent_List(Resource):
    def get(self):
        return generic_get_all(self, 'agent')
    def post(self):
        return generic_post_all(self, 'agent')
    def delete(self):
        return generic_delete_all(self, 'agent')

class Organization(Resource):
    def get(self, organization_key):
        return generic_get(self, organization_key, 'organization')
    def delete(self, organization_key):
        return generic_delete(self, organization_key, 'organization')
    def put(self, organization_key):
        return generic_put(self, organization_key, 'organization')

class Organization_List(Resource):
    def get(self):
        return generic_get_all(self, 'organization')
    def post(self):
        return generic_post_all(self, 'organization')
    def delete(self):
        return generic_delete_all(self, 'organization')

class Person(Resource):
    def get(self, person_key):
        return generic_get(self, person_key, 'person')
    def delete(self, person_key):
        return generic_delete(self, person_key, 'person')
    def put(self, person_key):
        return generic_put(self, person_key, 'person')

class Person_List(Resource):
    def get(self):
        return generic_get_all(self, 'person')
    def post(self):
        return generic_post_all(self, 'person')
    def delete(self):
        return generic_delete_all(self, 'person')

class SoftwareAgent(Resource):
    def get(self, software_agent_key):
        return generic_get(self, software_agent_key, 'software_agent')
    def delete(self, software_agent_key):
        return generic_delete(self, software_agent_key, 'software_agent')
    def put(self, software_agent_key):
        return generic_put(self, software_agent_key, 'software_agent')

class SoftwareAgent_List(Resource):
    def get(self):
        return generic_get_all(self, 'software_agent')
    def post(self):
        return generic_post_all(self, 'software_agent')
    def delete(self):
        return generic_delete_all(self, 'software_agent')


class Entity(Resource):
    def get(self, entity_key):
        return generic_get(self, entity_key, 'entity')
    def delete(self, entity_key):
        return generic_delete(self, entity_key, 'entity')
    def put(self, entity_key):
        return generic_put(self, entity_key, 'entity')

class Entity_List(Resource):
    def get(self):
        return generic_get_all(self, 'entity')
    def post(self):
        return generic_post_all(self, 'entity')
    def delete(self):
        return generic_delete_all(self, 'entity')

class Bundle(Resource):
    def get(self, bundle_key):
        return generic_get(self, bundle_key, 'bundle')
    def delete(self, bundle_key):
        return generic_delete(self, bundle_key, 'bundle')
    def put(self, bundle_key):
        return generic_put(self, bundle_key, 'bundle')

class Bundle_List(Resource):
    def get(self):
        return generic_get_all(self, 'bundle')
    def post(self):
        return generic_post_all(self, 'bundle')
    def delete(self):
        return generic_delete_all(self, 'bundle')

class Collection(Resource):
    def get(self, collection_key):
        return generic_get(self, collection_key, 'collection')
    def delete(self, collection_key):
        return generic_delete(self, collection_key, 'collection')
    def put(self, collection_key):
        return generic_put(self, collection_key, 'collection')

class Collection_List(Resource):
    def get(self):
        return generic_get_all(self, 'collection')
    def post(self):
        return generic_post_all(self, 'collection')
    def delete(self):
        return generic_delete_all(self, 'collection')

class EmptyCollection(Resource):
    def get(self, empty_collection_key):
        return generic_get(self, empty_collection_key, 'empty_collection')
    def delete(self, empty_collection_key):
        return generic_delete(self, empty_collection_key, 'empty_collection')
    def put(self, empty_collection_key):
        return generic_put(self, empty_collection_key, 'empty_collection')

class EmptyCollection_List(Resource):
    def get(self):
        return generic_get_all(self, 'empty_collection')
    def post(self):
        return generic_post_all(self, 'empty_collection')
    def delete(self):
        return generic_delete_all(self, 'empty_collection')

class Plan(Resource):
    def get(self, plan_key):
        return generic_get(self, plan_key, 'empty_collection')
    def delete(self, plan_key):
        return generic_delete(self, plan_key, 'empty_collection')
    def put(self, plan_key):
        return generic_put(self, plan_key, 'empty_collection')

class Plan_List(Resource):
    def get(self):
        return generic_get_all(self, 'plan')
    def post(self):
        return generic_post_all(self, 'plan')
    def delete(self):
        return generic_delete_all(self, 'plan')

class Influence(Resource):
    def get(self, influence_key):
        return generic_get(self, influence_key, 'influence')
    def delete(self, influence_key):
        return generic_delete(self, influence_key, 'influence')
    def put(self, influence_key):
        return generic_put(self, influence_key, 'influence')

class Influence_List(Resource):
    def get(self):
        return generic_get_all(self, 'influence')
    def post(self):
        return generic_post_all(self, 'influence')
    def delete(self):
        return generic_delete_all(self, 'influence')

class Activity_Influence(Resource):
    def get(self, activity_influence_key):
        return generic_get(self, activity_influence_key, 'activity_influence')
    def delete(self, activity_influence_key):
        return generic_delete(self, activity_influence_key, 'activity_influence')
    def put(self, activity_influence_key):
        return generic_put(self, activity_influence_key, 'activity_influence')

class Activity_Influence_List(Resource):
    def get(self):
        return generic_get_all(self, 'activity_influence')
    def post(self):
        return generic_post_all(self, 'activity_influence')
    def delete(self):
        return generic_delete_all(self, 'activity_influence')

class Communication(Resource):
    def get(self, communication_key):
        return generic_get(self, communication_key, 'communication')
    def delete(self, communication_key):
        return generic_delete(self, communication_key, 'communication')
    def put(self, communication_key):
        return generic_put(self, communication_key, 'communication')

class Communication_List(Resource):
    def get(self):
        return generic_get_all(self, 'communication')
    def post(self):
        return generic_post_all(self, 'communication')
    def delete(self):
        return generic_delete_all(self, 'communication')

class Generation(Resource):
    def get(self, generation_key):
        return generic_get(self, generation_key, 'generation')
    def delete(self, generation_key):
        return generic_delete(self, generation_key, 'generation')
    def put(self, generation_key):
        return generic_put(self, generation_key, 'generation')

class Generation_List(Resource):
    def get(self):
        return generic_get_all(self, 'generation')
    def post(self):
        return generic_post_all(self, 'generation')
    def delete(self):
        return generic_delete_all(self, 'generation')

class Invalidation(Resource):
    def get(self, invalidation_key):
        return generic_get(self, invalidation_key, 'invalidation')
    def delete(self, invalidation_key):
        return generic_delete(self, invalidation_key, 'invalidation')
    def put(self, invalidation_key):
        return generic_put(self, invalidation_key, 'invalidation')

class Invalidation_List(Resource):
    def get(self):
        return generic_get_all(self, 'invalidation')
    def post(self):
        return generic_post_all(self, 'invalidation')
    def delete(self):
        return generic_delete_all(self, 'invalidation')

class Agent_Influence(Resource):
    def get(self, agent_influence_key):
        return generic_get(self, agent_influence_key, 'agent_influence')
    def delete(self, agent_influence_key):
        return generic_delete(self, agent_influence_key, 'agent_influence')
    def put(self, agent_influence_key):
        return generic_put(self, agent_influence_key, 'agent_influence')

class Agent_Influence_List(Resource):
    def get(self):
        return generic_get_all(self, 'agent_influence')
    def post(self):
        return generic_post_all(self, 'agent_influence')
    def delete(self):
        return generic_delete_all(self, 'agent_influence')

class Association(Resource):
    def get(self, association_key):
        return generic_get(self, association_key, 'association')
    def delete(self, association_key):
        return generic_delete(self, association_key, 'association')
    def put(self, association_key):
        return generic_put(self, association_key, 'association')

class Association_List(Resource):
    def get(self):
        return generic_get_all(self, 'association')
    def post(self):
        return generic_post_all(self, 'association')
    def delete(self):
        return generic_delete_all(self, 'association')

class Attribution(Resource):
    def get(self, attribution_key):
        return generic_get(self, attribution_key, 'attribution')
    def delete(self, attribution_key):
        return generic_delete(self, attribution_key, 'attribution')
    def put(self, attribution_key):
        return generic_put(self, attribution_key, 'attribution')

class Attribution_List(Resource):
    def get(self):
        return generic_get_all(self, 'attribution')
    def post(self):
        return generic_post_all(self, 'attribution')
    def delete(self):
        return generic_delete_all(self, 'attribution')

class Delegation(Resource):
    def get(self, delegation_key):
        return generic_get(self, delegation_key, 'delegation')
    def delete(self, delegation_key):
        return generic_delete(self, delegation_key, 'delegation')
    def put(self, delegation_key):
        return generic_put(self, delegation_key, 'delegation')

class Delegation_List(Resource):
    def get(self):
        return generic_get_all(self, 'delegation')
    def post(self):
        return generic_post_all(self, 'delegation')
    def delete(self):
        return generic_delete_all(self, 'delegation')

class Entity_Influence(Resource):
    def get(self, entity_influence_key):
        return generic_get(self, entity_influence_key, 'entity_influence')
    def delete(self, entity_influence_key):
        return generic_delete(self, entity_influence_key, 'entity_influence')
    def put(self, entity_influence_key):
        return generic_put(self, entity_influence_key, 'entity_influence')

class Entity_Influence_List(Resource):
    def get(self):
        return generic_get_all(self, 'entity_influence')
    def post(self):
        return generic_post_all(self, 'entity_influence')
    def delete(self):
        return generic_delete_all(self, 'entity_influence')

class Derivation(Resource):
    def get(self, derivation_key):
        return generic_get(self, derivation_key, 'derivation')
    def delete(self, derivation_key):
        return generic_delete(self, derivation_key, 'derivation')
    def put(self, derivation_key):
        return generic_put(self, derivation_key, 'derivation')

class Derivation_List(Resource):
    def get(self):
        return generic_get_all(self, 'derivation')
    def post(self):
        return generic_post_all(self, 'derivation')
    def delete(self):
        return generic_delete_all(self, 'derivation')

class PrimarySource(Resource):
    def get(self, primary_source_key):
        return generic_get(self, primary_source_key, 'primary_source')
    def delete(self, primary_source_key):
        return generic_delete(self, primary_source_key, 'primary_source')
    def put(self, primary_source_key):
        return generic_put(self, primary_source_key, 'primary_source')

class PrimarySource_List(Resource):
    def get(self):
        return generic_get_all(self, 'primary_source')
    def post(self):
        return generic_post_all(self, 'primary_source')
    def delete(self):
        return generic_delete_all(self, 'primary_source')

class Quotation(Resource):
    def get(self, quotation_key):
        return generic_get(self, quotation_key, 'quotation')
    def delete(self, quotation_key):
        return generic_delete(self, quotation_key, 'quotation')
    def put(self, quotation_key):
        return generic_put(self, quotation_key, 'quotation')

class Quotation_List(Resource):
    def get(self):
        return generic_get_all(self, 'quotation')
    def post(self):
        return generic_post_all(self, 'quotation')
    def delete(self):
        return generic_delete_all(self, 'quotation')

class Revision(Resource):
    def get(self, revision_key):
        return generic_get(self, revision_key, 'revision')
    def delete(self, revision_key):
        return generic_delete(self, revision_key, 'revision')
    def put(self, revision_key):
        return generic_put(self, revision_key, 'revision')

class Revision_List(Resource):
    def get(self):
        return generic_get_all(self, 'revision')
    def post(self):
        return generic_post_all(self, 'revision')
    def delete(self):
        return generic_delete_all(self, 'revision')

class End(Resource):
    def get(self, end_key):
        return generic_get(self, end_key, 'ends')
    def delete(self, end_key):
        return generic_delete(self, end_key, 'ends')
    def put(self, end_key):
        return generic_put(self, end_key, 'ends')

class End_List(Resource):
    def get(self):
        return generic_get_all(self, 'ends')
    def post(self):
        return generic_post_all(self, 'ends')
    def delete(self):
        return generic_delete_all(self, 'ends')

class Start(Resource):
    def get(self, start_key):
        return generic_get(self, start_key, 'start')
    def delete(self, start_key):
        return generic_delete(self, start_key, 'start')
    def put(self, start_key):
        return generic_put(self, start_key, 'start')

class Start_List(Resource):
    def get(self):
        return generic_get_all(self, 'start')
    def post(self):
        return generic_post_all(self, 'start')
    def delete(self):
        return generic_delete_all(self, 'start')

class Usage(Resource):
    def get(self, usage_key):
        return generic_get(self, usage_key, 'usage')
    def delete(self, usage_key):
        return generic_delete(self, usage_key, 'usage')
    def put(self, usage_key):
        return generic_put(self, usage_key, 'usage')

class Usage_List(Resource):
    def get(self):
        return generic_get_all(self, 'usage')
    def post(self):
        return generic_post_all(self, 'usage')
    def delete(self):
        return generic_delete_all(self, 'usage')

class Instantaneous_Event(Resource):
    def get(self, instantaneous_event_key):
        return generic_get(self, instantaneous_event_key, 'instantaneous_event')
    def delete(self, instantaneous_event_key):
        return generic_delete(self, instantaneous_event_key, 'instantaneous_event')
    def put(self, instantaneous_event_key):
        return generic_put(self, instantaneous_event_key, 'instantaneous_event')

class Instantaneous_Event_List(Resource):
    def get(self):
        return generic_get_all(self, 'instantaneous_event')
    def post(self):
        return generic_post_all(self, 'instantaneous_event')
    def delete(self):
        return generic_delete_all(self, 'instantaneous_event')

class Location(Resource):
    def get(self, location_key):
        return generic_get(self, location_key, 'location')
    def delete(self, location_key):
        return generic_delete(self, location_key, 'location')
    def put(self, location_key):
        return generic_put(self, location_key, 'location')

class Location_List(Resource):
    def get(self):
        return generic_get_all(self, 'location')
    def post(self):
        return generic_post_all(self, 'location')
    def delete(self):
        return generic_delete_all(self, 'location')

class Role(Resource):
    def get(self, role_key):
        return generic_get(self, role_key, 'role')
    def delete(self, role_key):
        return generic_delete(self, role_key, 'role')
    def put(self, role_key):
        return generic_put(self, role_key, 'role')

class Role_List(Resource):
    def get(self):
        return generic_get_all(self, 'role')
    def post(self):
        return generic_post_all(self, 'role')
    def delete(self):
        return generic_delete_all(self, 'role')

class Domain(Resource):
    def get(self, domain_id):
        is_domains_for = []
        if not domain_id in fields.keys():
            abort(404, message='Invalid type requested: ' + domain_id)
        for foreign_key in foreign_keys[domain_id]:
            is_domains_for.append(foreign_key)
        return list(set(is_domains_for)), 200

class Domain_List(Resource):
    def get(self):
        result = {}
        for type_id in fields.keys():
            is_domains_for = []
            if type_id in foreign_keys.keys():
                for foreign_key in foreign_keys[type_id]:
                    is_domains_for.append(foreign_key)
            result[type_id] = list(set(is_domains_for))
        return result, 200

class Codomain(Resource):
    def get(self, codomain_id):
        if not codomain_id in fields.keys():
            abort(404, message='Invalid type requested: ' + domain_id)
        is_codomains_for = []
        for foreign_key in foreign_keys.keys():
            domains, codomains = io(foreign_key)
            if codomain_id in codomains:
                is_ccodomains_for.append(foreign_key)
        return is_codomains_for, 200

class Codomain_List(Resource):
    def get(self):
        result = {}
        for type_id in fields.keys():
            for foreign_key in foreign_keys.keys():
                is_codomains_for = []
                domains, codomains = io(foreign_key)
            result[foreign_key] = is_codomains_for
        return result, 200
 
class ObjectProperty(Resource):
    def get(self, object_property):
        if not object_property in valid_foreign_keys:
            abort(404, message='Invalid object property requested: ' + object_property)
        domains,codomains = io(object_property)
        relations = { 
            'domains': domains,
            'codomains': codomains
            }
        return relations, 200


class ObjectProperty_List(Resource):
    def get(self):
        relations = {}
        for foreign_key in valid_foreign_keys:
            domains, codomains = io(foreign_key) 
            relations[foreign_key] = { 
                'domains': domains,
                'codomains': codomains
                }
        return relations, 200

class Class_List(Resource):
    def get(self):
        return fields.keys()

def reification(source, target, relation):

    return '_' + source + '2' + target + '4' + relation

api.add_resource(Activity, '/prov-o/activity/<activity_key>')
api.add_resource(Activity_List, '/prov-o/activity')
api.add_resource(Agent, '/prov-o/agent/<agent_key>')
api.add_resource(Agent_List, '/prov-o/agent')
api.add_resource(Organization, '/prov-o/organization/<organization_key>')
api.add_resource(Organization_List, '/prov-o/organization')
api.add_resource(Person, '/prov-o/person/<person_key>')
api.add_resource(Person_List, '/prov-o/person')
api.add_resource(SoftwareAgent, '/prov-o/software_agent/<software_agent_key>')
api.add_resource(SoftwareAgent_List, '/prov-o/software_agent')
api.add_resource(Entity, '/prov-o/entity/<entity_key>')
api.add_resource(Entity_List, '/prov-o/entity')
api.add_resource(Bundle, '/prov-o/bundle/<bundle_key>')
api.add_resource(Bundle_List, '/prov-o/bundle')
api.add_resource(Collection, '/prov-o/collection/<collection_key>')
api.add_resource(Collection_List, '/prov-o/collection')
api.add_resource(EmptyCollection, '/prov-o/empty_collection/<empty_collection_key>')
api.add_resource(EmptyCollection_List, '/prov-o/empty_collection')
api.add_resource(Plan, '/prov-o/plan/<plan_key>')
api.add_resource(Plan_List, '/prov-o/plan')
api.add_resource(Influence, '/prov-o/influence/<influence_key>')
api.add_resource(Influence_List, '/prov-o/influence')
api.add_resource(Activity_Influence, '/prov-o/activity_influence/<activity_influence_key>')
api.add_resource(Activity_Influence_List, '/prov-o/activity_influence')
api.add_resource(Generation, '/prov-o/generation/<generation_key>')
api.add_resource(Generation_List, '/prov-o/generation')
api.add_resource(Communication, '/prov-o/communication/<communication_key>')
api.add_resource(Communication_List, '/prov-o/communication')
api.add_resource(Invalidation, '/prov-o/invalidation/<invalidation_key>')
api.add_resource(Invalidation_List, '/prov-o/invalidation')
api.add_resource(Agent_Influence, '/prov-o/agent_influence/<agent_influence_key>')
api.add_resource(Agent_Influence_List, '/prov-o/agent_influence')
api.add_resource(Association, '/prov-o/association/<association_key>')
api.add_resource(Association_List, '/prov-o/association')
api.add_resource(Attribution, '/prov-o/attribution/<attribution_key>')
api.add_resource(Attribution_List, '/prov-o/attribution')
api.add_resource(Delegation, '/prov-o/delegation/<delegation_key>')
api.add_resource(Delegation_List, '/prov-o/delegation')
api.add_resource(Entity_Influence, '/prov-o/entity_influence/<entity_influence_key>')
api.add_resource(Entity_Influence_List, '/prov-o/entity_influence')
api.add_resource(Derivation, '/prov-o/derivation/<derivation_key>')
api.add_resource(Derivation_List, '/prov-o/derivation')
api.add_resource(PrimarySource, '/prov-o/primary_source/<primary_source_key>')
api.add_resource(PrimarySource_List, '/prov-o/primary_source')
api.add_resource(Quotation, '/prov-o/quotation/<quotation_key>')
api.add_resource(Quotation_List, '/prov-o/quotation')
api.add_resource(Revision, '/prov-o/revision/<revision_key>')
api.add_resource(Revision_List, '/prov-o/revision')
api.add_resource(End, '/prov-o/end/<end_key>')
api.add_resource(End_List, '/prov-o/end')
api.add_resource(Start, '/prov-o/start/<start_key>')
api.add_resource(Start_List, '/prov-o/start')
api.add_resource(Usage, '/prov-o/usage/<usage_key>')
api.add_resource(Usage_List, '/prov-o/usage')
api.add_resource(Instantaneous_Event, '/prov-o/instantaneous_event/<instantaneous_event_key>')
api.add_resource(Instantaneous_Event_List, '/prov-o/instantaneous_event')
api.add_resource(Location, '/prov-o/location/<location_key>')
api.add_resource(Location_List, '/prov-o/location')
api.add_resource(Role, '/prov-o/role/<role_key>')
api.add_resource(Role_List, '/prov-o/role')
api.add_resource(Domain_List, '/prov-o/domain')
api.add_resource(Domain, '/prov-o/domain/<domain_id>')
api.add_resource(Codomain_List, '/prov-o/codomain')
api.add_resource(Codomain, '/prov-o/codomain/<codomain_id>')
api.add_resource(ObjectProperty, '/prov-o/object_property/<object_property>')
api.add_resource(ObjectProperty_List, '/prov-o/object_property')

if __name__ == '__main__':
    if len(sys.argv) > 1 and sys.argv[1] == "tables":
        print_tables()
        sys.exit()
    if len(sys.argv) > 1 and sys.argv[1] == "drop":
        print_drop_tables()
        sys.exit()
    app.run(debug=True)




