#!/bin/bash

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

# An automatic script to generate the reference implementation for accessing
# the database.

# This is an extremely basic workflow. There will be:

# + an all query
# + a delete query
# + an update query
# + an insert query

# Doug Salt

# June 2020


TYPES="Activity Agent Organization Person Software_Agent Entity Bundle Collection Empty_Collection Plan Influence Activity_Influence Communication Generation Invalidation Agent_Influence Association Attribution Delegation Entity_Influence Derivation Primary_Source Quotation Revision Ends Start Usage Instantaneous_Event Location Role"
#TYPES="Agent"
for type in $TYPES
do
    lowercase_type=$(echo $type | tr '[:upper:]' '[:lower:]')
    type=$(echo "$type" | sed "s/_/ /g")
    active="true"
    if [ "$type" = 'Location' ] \
    || [ "$type" = 'Role' ]
    then
        active='false'
    fi
    for action in "all" "delete" "insert" "update"
    do
        if  [ "$action" = 'update' ] \
        && ([ "$type" = 'Location' ] \
        ||  [ "$type" = 'Role' ])
        then
            echo """ <!doctype html>
<html lang='en'>
    <head>
          <meta charset='utf-8'>
          <meta name='viewport' content='width=device-width, initial-scale=1.0'>
          <meta http-equiv='refresh' content='0; url=/all-${lowercase_type}.html' />
          <title>Doug's provenance application</title>
          <link href='https://fonts.googleapis.com/css?family=Dosis:400,700' rel='stylesheet'>
          <link href='style.css' rel='stylesheet'>
    </head>
    <body>
        <p><a href='/all-${lowercase_type}.html'>Redirect</a>
    </body>
</html>
""" >  ${action}-${lowercase_type}.html
            continue
        fi
        echo """ <!doctype html>
<html lang='en'>
    <head>
          <meta charset='utf-8'>
          <meta name='viewport' content='width=device-width, initial-scale=1.0'>
          <title>Doug's provenance application</title>
          <link href='https://fonts.googleapis.com/css?family=Dosis:400,700' rel='stylesheet'>
          <link href='style.css' rel='stylesheet'>
    </head>


    <body>
          <div id='root'></div>
          <script>
            const TYPE = '$type';
            const ACTION = '$action';
            const ACTIVE = $active;
          </script>
          <script src='common.js'></script>
          <script src='${action}.js'></script>
    </body>

</html>""" > ${action}-${lowercase_type}.html
    done
done

echo """<!doctype html>
<html lang="en">
    <head>
          <meta charset="utf-8">
          <meta name="viewport" content="width=device-width, initial-scale=1.0">
          <title>Dougs Provenance App</title>
          <link href="https://fonts.googleapis.com/css?family=Dosis:400,700" rel="stylesheet">
          <link href="style.css" rel="stylesheet"> 
    </head>


    <body>
          <div id="root"></div>
          <script src="index.js"></script>
    </body>

</html>""" > index.html

echo """const app = document.getElementById('root');

var title = document.createElement('div');

const logo = document.createElement('img');
logo.setAttribute('class', 'logo');
logo.src = 'logo.png';

const h1 = document.createElement('h1');
h1.textContent = 'Provenance Database';

const container = document.createElement('div');

title.appendChild(logo);
title.appendChild(h1);

app.appendChild(title);
app.appendChild(container);

const table = document.createElement('table');
container.appendChild(table);

var td = []
var tr = [];

""" > index.js

row=0
for type in ${TYPES}
do
    lowercase_type=$(echo $type | tr '[:upper:]' '[:lower:]')
    level=1
    if [ "$type" = 'Activity' ] \
    || [ "$type" = 'Agent' ] \
    || [ "$type" = 'Entity' ] \
    || [ "$type" = 'Influence' ] \
    || [ "$type" = 'Instantaneous_Event' ] \
    || [ "$type" = 'Location' ] \
    || [ "$type" = 'Role' ]
    then
        level=0
    fi
    if [ "$type" = 'Empty_Collection' ] \
    || [ "$type" = 'Communication' ] \
    || [ "$type" = 'Generation' ] \
    || [ "$type" = 'Invalidation' ] \
    || [ "$type" = 'Association' ] \
    || [ "$type" = 'Attribution' ] \
    || [ "$type" = 'Delegation' ] \
    || [ "$type" = 'Derivation' ] \
    || [ "$type" = 'Ends' ] \
    || [ "$type" = 'Start' ] \
    || [ "$type" = 'Usage' ]
    then
        level=2
    fi
    if [ "$type" = 'Primary_Source' ] \
    || [ "$type" = 'Quotation' ] \
    || [ "$type" = 'Revision' ]
    then
        level=3
    fi
    type=$(echo $type | sed "s/_/ /g")
    echo """
tr[${row}] = table.insertRow(-1);
td[${row}] = [];
td[${row}][0] = tr[${row}].insertCell(-1);
td[${row}][1] = tr[${row}].insertCell(-1);
td[${row}][2] = tr[${row}].insertCell(-1);
td[${row}][3] = tr[${row}].insertCell(-1);
td[${row}][4] = tr[${row}].insertCell(-1);
td[${row}][5] = tr[${row}].insertCell(-1);

const all_${lowercase_type}  = document.createElement('input');
all_${lowercase_type}.setAttribute('type','submit');
all_${lowercase_type}.setAttribute('value','Every ${type}');
all_${lowercase_type}.onclick = function(){window.location = '/all-${lowercase_type}.html'};

const insert_${lowercase_type}  = document.createElement('input');
insert_${lowercase_type}.setAttribute('type','submit');
insert_${lowercase_type}.setAttribute('value','Insert ${type}');
insert_${lowercase_type}.onclick = function(){window.location = '/insert-${lowercase_type}.html'};

const remove_${lowercase_type}  = document.createElement('input');
remove_${lowercase_type}.setAttribute('type','submit');
remove_${lowercase_type}.setAttribute('value','Delete ${type}');
remove_${lowercase_type}.onclick = function(){window.location = '/delete-${lowercase_type}.html'};
""" >> index.js

    if [ $level = 0 ]
    then
        echo """
    td[${row}][0].appendChild(all_${lowercase_type});
    td[${row}][1].appendChild(insert_${lowercase_type});
    td[${row}][2].appendChild(remove_${lowercase_type});
""" >> index.js
    else
        echo """
const ${lowercase_type}_bent_arrow = document.createElement('img');
${lowercase_type}_bent_arrow.src = 'bent.png'
""" >> index.js
        if [ $level = 1 ]
        then
            echo """
td[${row}][0].appendChild(${lowercase_type}_bent_arrow);
td[${row}][0].setAttribute('align', 'right');
td[${row}][1].appendChild(all_${lowercase_type});
td[${row}][2].appendChild(insert_${lowercase_type});
td[${row}][3].appendChild(remove_${lowercase_type});
""" >> index.js
        elif [ "$type" = 'Derivation' -o  "$type" = 'Ends' -o "$type" = 'Start' -o "$type" = 'Usage' ]
        then
            echo """
const ${lowercase_type}_vertical = document.createElement('img');
${lowercase_type}_vertical.src = 'vertical.png'
td[${row}][0].innerHTML = '</nbsp>';
td[${row}][1].appendChild(${lowercase_type}_bent_arrow);
td[${row}][1].setAttribute('align', 'right');
td[${row}][2].appendChild(all_${lowercase_type});
td[${row}][3].appendChild(insert_${lowercase_type});
td[${row}][4].appendChild(remove_${lowercase_type});
""" >> index.js
        elif [ $level = 2 ]
        then
            echo """
const ${lowercase_type}_vertical = document.createElement('img');
${lowercase_type}_vertical.src = 'vertical.png'
td[${row}][0].appendChild(${lowercase_type}_vertical);
td[${row}][0].setAttribute('align', 'right');
td[${row}][1].appendChild(${lowercase_type}_bent_arrow);
td[${row}][1].setAttribute('align', 'right');
td[${row}][2].appendChild(all_${lowercase_type});
td[${row}][3].appendChild(insert_${lowercase_type});
td[${row}][4].appendChild(remove_${lowercase_type});
""" >> index.js
        else
            echo """
td[${row}][0].innerHTML = '<\/nbsp>';
const ${lowercase_type}_vertical_other = document.createElement('img');
${lowercase_type}_vertical_other.src = 'vertical.png'
td[${row}][1].appendChild(${lowercase_type}_vertical_other);
td[${row}][1].setAttribute('align', 'right');
td[${row}][2].appendChild(${lowercase_type}_bent_arrow);
td[${row}][2].setAttribute('align', 'right');
td[${row}][3].appendChild(all_${lowercase_type});
td[${row}][4].appendChild(insert_${lowercase_type});
td[${row}][5].appendChild(remove_${lowercase_type});
""" >> index.js
        fi
    fi

done
