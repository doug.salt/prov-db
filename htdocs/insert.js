const table = document.createElement('table');
container.appendChild(table)

const h2Label = document.createElement('h2');
h2Label.textContent = 'Name: ';

const name = document.createElement('input'); 
name.setAttribute('type','text');
name.setAttribute('name','name');

const submitButton  = document.createElement('input');
submitButton.setAttribute('type','submit');
submitButton.setAttribute('value','Add');

var tr = [];
var td = [];
var row = 0;

tr[row] = table.insertRow(0);
td[row] = [];
td[row][0] = tr[row].insertCell(-1);
td[row][1] = tr[row].insertCell(-1);
td[row][2] = tr[row].insertCell(-1);
td[row][3] = tr[row].insertCell(-1);

td[row][0].appendChild(h2Label);
td[row][1].appendChild(name);
td[row][2].appendChild(submitButton);

var insert = function() {
	var request = new XMLHttpRequest();
    url = '/prov-o/' + lowerCaseType + 
        '/' + name.value;
    result = scrape();
	request.open('PUT', url, true);	
    request.setRequestHeader("Content-Type", "application/json")
	request.onload = function() {
		if (request.status >= 200 && request.status < 400) {
			window.location= '/all-' + 
                lowerCaseType + '.html';
		} else {
            console.log(url + ', GET: ' + request.status + 
            ' with ' + request.message)
		}
	}
    console.log(result);
	request.send(JSON.stringify(result));
}

submitButton.onclick = insert;

if (TYPE == 'Instantaneous Event' 
||  TYPE == 'Ends' 
||  TYPE == 'Generation' 
||  TYPE == 'Invalidation' 
||  TYPE == 'Start' 
||  TYPE == 'Usage') 
    inputField('at_time',null,validStartedAtTime);
if (TYPE == 'Activity') 
    inputField('ended_at_time',null,validStartedAtTime);
if (TYPE == 'Entity' 
||  TYPE == 'Bundle' 
||  TYPE == 'Collection' 
||  TYPE == 'Empty Collection' 
||  TYPE == 'Plan' )
    inputField('generated_at_time',null,validStartedAtTime);
if (TYPE == 'Entity' 
||  TYPE == 'Bundle' 
||  TYPE == 'Collection' 
||  TYPE == 'Empty Collection' 
||  TYPE == 'Plan' )
    inputField('invalidated_at_time',null,validStartedAtTime);
if (TYPE == 'Activity')
    inputField('started_at_time',null,validStartedAtTime);
if (TYPE == 'Entity' 
||  TYPE == 'Bundle' 
||  TYPE == 'Collection' 
||  TYPE == 'Empty Collection' 
||  TYPE == 'Plan' )
    inputField('has_value',null);
if (TYPE != 'Activity' 
&&  TYPE != 'Agent' 
&&  TYPE != 'Organization' 
&&  TYPE != 'Person' 
&&  TYPE != 'Software Agent' 
&&  TYPE != 'Entity' 
&&  TYPE != 'Bundle' 
&&  TYPE != 'Collection'
&&  TYPE != 'Empty Collection' 
&&  TYPE != 'Plan' 
&&  TYPE != 'Role'
&&  TYPE != 'Location')
    inputField('infuencer',null,validStartedAtTime);

row ++;





