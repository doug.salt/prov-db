const put = function() {
	var request = new XMLHttpRequest();
    const put = '/prov-o/' + lowerCaseType +'/' + name; 
	request.open('PUT', put, true);	
    request.setRequestHeader("Content-Type", "application/json")
    var result = scrape();
    if (select.selectedIndex > 0 && chooseCodomain.selectedIndex > 0) {
        if (result[select.value] == null)
            result[select.value] = [];
        result[select.value].push(chooseCodomain.value);
    }
	request.onload = function() {
		if (request.status >= 200 && request.status < 400) {
            row = 0;
            container.removeChild(table);
			window.location = '/update-' + lowerCaseType + 
                '.html?name=' + name;
		} else {
            console.log('error: ' + request.status + ' with ' + request.message)
		}
	}
    console.log(JSON.stringify(result));
	request.send(JSON.stringify(result));
}

const codomains = function() {
    foreign_keys[select.value].codomains.forEach(codomain => {
        var request = new XMLHttpRequest();
        request.open('GET', '/prov-o/' + codomain , true);	
        request.onload = function() {
            data = JSON.parse(this.response);
            if (request.status >= 200 && request.status < 400) {
                data.forEach(possible_domain => {
                    var opt = document.createElement('option');
                    opt.value = codomain + '(' + possible_domain.name + ')';
                    opt.innerHTML = codomain + '(' + possible_domain.name + ')';
                    chooseCodomain.appendChild(opt);
                })
            } else {
                console.log('/prov-o/' + codomain + 
                        ', get: ' + request.status + 
                        ' with ' + request.message)
            }
        }
        request.send();
    })
}

const removeRow = function() {
    let removeThisRow = this.parentElement.parentElement.rowIndex;
    table.deleteRow(removeThisRow);
    row --;
    for (i = removeThisRow; i < row; i++ ) {
        tr[i] = tr[i + 1];
        td[i] = td[i + 1];
    }
}

var name = findGetParameter('name');

const table = document.createElement('table');
const h2 = document.createElement('h2');
h2.textContent = 'Name: ' + name 

const select = document.createElement('select'); 
emptyRel = document.createElement('option');
emptyRel.value = ''
emptyRel.innerHTML = ''
select.appendChild(emptyRel);
select.onchange = codomains;

container.appendChild(table);

const submitButton  = document.createElement('input');
submitButton.setAttribute('type','submit');
submitButton.setAttribute('value','Update');
submitButton.onclick = put;

var chooseCodomain = document.createElement('select'); 
empty = document.createElement('option');
empty.value = ''
empty.innerHTML = ''
chooseCodomain.appendChild(empty);

var foreign_keys;

var tr = [];
var td = [];
var row = 0;

tr[row] = table.insertRow(0);
td[row] =[];
td[row][0] = tr[row].insertCell(-1);
td[row][1] = tr[row].insertCell(-1);
td[row][2] = tr[row].insertCell(-1);
td[row][3] = tr[row].insertCell(-1);
td[row][4] = tr[row].insertCell(-1);

td[row][0].appendChild(h2);
td[row][1].appendChild(submitButton);

var request = new XMLHttpRequest();
var url = '/prov-o/' + lowerCaseType + '/' + name;

var domains_request;

request.open('GET', url, true);

request.onload = function () {
    var data = JSON.parse(this.response);
    if (request.status >= 200 && request.status < 400) {
        for (key in data) {
            if (key == 'started_at_time') {
                inputField(key, data[key], validStartedAtTime);
            }
            else if (key == 'ended_at_time') {
                inputField(key, data[key], validEndedAtTime);
            }
            else if (key == 'generated_at_time') {
                inputField(key, data[key], validEndedAtTime);
            }
            else if (key == 'invalidated_at_time') {
                inputField(key, data[key], validEndedAtTime);
            }
            else if (key == 'at_time') {
                inputField(key, data[key], validEndedAtTime);
            }
            else if (key == 'has_value') {
                inputField(key, data[key]);
            }
            else if (key == 'influencer') {
                inputField(key, data[key]);
            }
            else if (key != 'name') {
                for (i = 0; i < data[key].length; i++) {
                    row ++;
                    tr[row] = table.insertRow(row);
                    td[row] = [];
                    td[row][0] = tr[row].insertCell(-1);
                    td[row][1] = tr[row].insertCell(-1);
                    td[row][2] = tr[row].insertCell(-1);
                    td[row][3] = tr[row].insertCell(-1);
                    td[row][4] = tr[row].insertCell(-1);

                    const keyBentArrow = document.createElement('img');
                    keyBentArrow.src = 'bent.png';
                    td[row][0].appendChild(keyBentArrow);
                    td[row][0].setAttribute('align', 'right');
                    td[row][1].innerHTML = key;
                    const keyStraightArrow = document.createElement('img');
                    keyStraightArrow.src = 'straight.png';
                    td[row][2].appendChild(keyStraightArrow);
                    a = document.createElement('a');
                    let match = data[key][i].match(/^(.*)\((.*)\)$/);
                    a.href = '/update-' + match[1] +
                        '.html?name=' + match[2];
                    a.innerHTML = data[key][i];
                    td[row][3].appendChild(a);
                    //td[row][3].innerHTML = data[key][i];
                    remove = document.createElement('input');
                    remove.setAttribute('type','submit');
                    remove.setAttribute('value','Remove');
                    remove.onclick = removeRow;
                    td[row][4].appendChild(remove);
                }
            }
        }

        domains_request = new XMLHttpRequest();

        domains_request.open('GET', '/prov-o/domain/' + lowerCaseType, true);

        domains_request.onload = function () {
            data = JSON.parse(this.response);
            if (request.status >= 200 && request.status < 400) {
                for (i = 0; i < data.length; i++ ) {
                    var opt = document.createElement('option');
                    opt.value = data[i];
                    opt.innerHTML = data[i];
                    select.appendChild(opt);
                }

                row++;
                tr[row] = table.insertRow(row);
                td[row] = [];
                td[row][0] = tr[row].insertCell(-1);
                td[row][1] = tr[row].insertCell(-1);
                td[row][2] = tr[row].insertCell(-1);
                td[row][3] = tr[row].insertCell(-1);
                td[row][4] = tr[row].insertCell(-1);

                const bentArrow = document.createElement('img');
                bentArrow.src = 'bent.png';

                const straightArrow = document.createElement('img');
                straightArrow.src = 'straight.png';

                td[row][0].appendChild(bentArrow);
                td[row][0].setAttribute('align','right');
                td[row][1].appendChild(select)
                td[row][2].appendChild(straightArrow);

                request = new XMLHttpRequest();
                url = '/prov-o/object_property'
                request.open('GET', url, true);
                request.onload = function() {
                    data = JSON.parse(this.response);
                    if (request.status >= 200 && request.status < 400) {
                        foreign_keys = data
                        td[row][3].appendChild(chooseCodomain);

                    } else {
                        console.log(url + ', GET: ' + request.status + ' with ' + request.message)
                    }
                }
                request.send();

            } else {
                console.log('/prov-o/domain/' + 
                        lowerCaseType + ', get: ' + 
                        request.status + ' with ' + 
                        request.message)
            }

        }
        domains_request.send();
    } else {
        console.log(url + ', GET: ' + request.status + 
                        ' with ' + request.message)
    }
}


request.send();




