const card = document.createElement('div');
card.setAttribute('class', 'card');

var select = document.createElement('select'); 
select.setAttribute('class','remove');

var request = new XMLHttpRequest();

request.open('GET', '/prov-o/' + lowerCaseType, true);

request.onload = function () {
  var data = JSON.parse(this.response);

  if (request.status >= 200 && request.status < 400) {
    data.forEach(entity => {
        var opt = document.createElement('option');
        opt.value = entity.name;
        opt.innerHTML = entity.name;
        select.appendChild(opt);
    })
  } else {
    console.log('error')
  }
}

const submitButton  = document.createElement('input');
submitButton.setAttribute('type','submit');
submitButton.setAttribute('value','Delete');
submitButton.setAttribute('class','remove');
submitButton.onclick = remove;
const h2 = document.createElement('h2');
h2.textContent = TYPE + ' name:';
h2.appendChild(select);
h2.appendChild(submitButton);

card.append(h2);
app.appendChild(card);

function remove() {
	var request = new XMLHttpRequest();
	request.open('DELETE', '/prov-o/' + lowerCaseType +'/' 
            + select.options[select.selectedIndex].value , true);	
	request.onload = function() {
		if (request.status >= 200 && request.status < 400) {
			window.location= '/all-' + 
                lowerCaseType + '.html';
		} else {
			console.log('error')
		}
	}
	request.send();
}

request.send();
