var request = new XMLHttpRequest();

const url = '/prov-o/' + lowerCaseType;
request.open('GET', url, true);

request.onload = function () {
  var data = JSON.parse(this.response);
  if (request.status >= 200 && request.status < 400) {
    data.forEach(entity => {
        var link;
        if (ACTIVE) {
            link = document.createElement('a');
            link.href = '/update-' + lowerCaseType + '.html?name=' + entity.name 
        }
        const card = document.createElement('div');
        card.setAttribute('class', 'card');

        const h1 = document.createElement('h1');
        h1.textContent = entity.name;

        card.appendChild(h1);
        if (ACTIVE) {
            link.appendChild(card);
            container.appendChild(link);
        } else
            container.appendChild(card);
    })
  } else {
    console.log(url + ', GET: ' + request.status + ' with ' + request.message)
  }
}

request.send();
