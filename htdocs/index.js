const app = document.getElementById('root');

var title = document.createElement('div');

const logo = document.createElement('img');
logo.setAttribute('class', 'logo');
logo.src = 'logo.png';

const h1 = document.createElement('h1');
h1.textContent = 'Provenance Database';

const container = document.createElement('div');

title.appendChild(logo);
title.appendChild(h1);

app.appendChild(title);
app.appendChild(container);

const table = document.createElement('table');
container.appendChild(table);

var td = []
var tr = [];



tr[0] = table.insertRow(-1);
td[0] = [];
td[0][0] = tr[0].insertCell(-1);
td[0][1] = tr[0].insertCell(-1);
td[0][2] = tr[0].insertCell(-1);
td[0][3] = tr[0].insertCell(-1);
td[0][4] = tr[0].insertCell(-1);
td[0][5] = tr[0].insertCell(-1);

const all_activity  = document.createElement('input');
all_activity.setAttribute('type','submit');
all_activity.setAttribute('value','Every Activity');
all_activity.onclick = function(){window.location = '/all-activity.html'};

const insert_activity  = document.createElement('input');
insert_activity.setAttribute('type','submit');
insert_activity.setAttribute('value','Insert Activity');
insert_activity.onclick = function(){window.location = '/insert-activity.html'};

const remove_activity  = document.createElement('input');
remove_activity.setAttribute('type','submit');
remove_activity.setAttribute('value','Delete Activity');
remove_activity.onclick = function(){window.location = '/delete-activity.html'};


    td[0][0].appendChild(all_activity);
    td[0][1].appendChild(insert_activity);
    td[0][2].appendChild(remove_activity);


tr[0] = table.insertRow(-1);
td[0] = [];
td[0][0] = tr[0].insertCell(-1);
td[0][1] = tr[0].insertCell(-1);
td[0][2] = tr[0].insertCell(-1);
td[0][3] = tr[0].insertCell(-1);
td[0][4] = tr[0].insertCell(-1);
td[0][5] = tr[0].insertCell(-1);

const all_agent  = document.createElement('input');
all_agent.setAttribute('type','submit');
all_agent.setAttribute('value','Every Agent');
all_agent.onclick = function(){window.location = '/all-agent.html'};

const insert_agent  = document.createElement('input');
insert_agent.setAttribute('type','submit');
insert_agent.setAttribute('value','Insert Agent');
insert_agent.onclick = function(){window.location = '/insert-agent.html'};

const remove_agent  = document.createElement('input');
remove_agent.setAttribute('type','submit');
remove_agent.setAttribute('value','Delete Agent');
remove_agent.onclick = function(){window.location = '/delete-agent.html'};


    td[0][0].appendChild(all_agent);
    td[0][1].appendChild(insert_agent);
    td[0][2].appendChild(remove_agent);


tr[0] = table.insertRow(-1);
td[0] = [];
td[0][0] = tr[0].insertCell(-1);
td[0][1] = tr[0].insertCell(-1);
td[0][2] = tr[0].insertCell(-1);
td[0][3] = tr[0].insertCell(-1);
td[0][4] = tr[0].insertCell(-1);
td[0][5] = tr[0].insertCell(-1);

const all_organization  = document.createElement('input');
all_organization.setAttribute('type','submit');
all_organization.setAttribute('value','Every Organization');
all_organization.onclick = function(){window.location = '/all-organization.html'};

const insert_organization  = document.createElement('input');
insert_organization.setAttribute('type','submit');
insert_organization.setAttribute('value','Insert Organization');
insert_organization.onclick = function(){window.location = '/insert-organization.html'};

const remove_organization  = document.createElement('input');
remove_organization.setAttribute('type','submit');
remove_organization.setAttribute('value','Delete Organization');
remove_organization.onclick = function(){window.location = '/delete-organization.html'};


const organization_bent_arrow = document.createElement('img');
organization_bent_arrow.src = 'bent.png'


td[0][0].appendChild(organization_bent_arrow);
td[0][0].setAttribute('align', 'right');
td[0][1].appendChild(all_organization);
td[0][2].appendChild(insert_organization);
td[0][3].appendChild(remove_organization);


tr[0] = table.insertRow(-1);
td[0] = [];
td[0][0] = tr[0].insertCell(-1);
td[0][1] = tr[0].insertCell(-1);
td[0][2] = tr[0].insertCell(-1);
td[0][3] = tr[0].insertCell(-1);
td[0][4] = tr[0].insertCell(-1);
td[0][5] = tr[0].insertCell(-1);

const all_person  = document.createElement('input');
all_person.setAttribute('type','submit');
all_person.setAttribute('value','Every Person');
all_person.onclick = function(){window.location = '/all-person.html'};

const insert_person  = document.createElement('input');
insert_person.setAttribute('type','submit');
insert_person.setAttribute('value','Insert Person');
insert_person.onclick = function(){window.location = '/insert-person.html'};

const remove_person  = document.createElement('input');
remove_person.setAttribute('type','submit');
remove_person.setAttribute('value','Delete Person');
remove_person.onclick = function(){window.location = '/delete-person.html'};


const person_bent_arrow = document.createElement('img');
person_bent_arrow.src = 'bent.png'


td[0][0].appendChild(person_bent_arrow);
td[0][0].setAttribute('align', 'right');
td[0][1].appendChild(all_person);
td[0][2].appendChild(insert_person);
td[0][3].appendChild(remove_person);


tr[0] = table.insertRow(-1);
td[0] = [];
td[0][0] = tr[0].insertCell(-1);
td[0][1] = tr[0].insertCell(-1);
td[0][2] = tr[0].insertCell(-1);
td[0][3] = tr[0].insertCell(-1);
td[0][4] = tr[0].insertCell(-1);
td[0][5] = tr[0].insertCell(-1);

const all_software_agent  = document.createElement('input');
all_software_agent.setAttribute('type','submit');
all_software_agent.setAttribute('value','Every Software Agent');
all_software_agent.onclick = function(){window.location = '/all-software_agent.html'};

const insert_software_agent  = document.createElement('input');
insert_software_agent.setAttribute('type','submit');
insert_software_agent.setAttribute('value','Insert Software Agent');
insert_software_agent.onclick = function(){window.location = '/insert-software_agent.html'};

const remove_software_agent  = document.createElement('input');
remove_software_agent.setAttribute('type','submit');
remove_software_agent.setAttribute('value','Delete Software Agent');
remove_software_agent.onclick = function(){window.location = '/delete-software_agent.html'};


const software_agent_bent_arrow = document.createElement('img');
software_agent_bent_arrow.src = 'bent.png'


td[0][0].appendChild(software_agent_bent_arrow);
td[0][0].setAttribute('align', 'right');
td[0][1].appendChild(all_software_agent);
td[0][2].appendChild(insert_software_agent);
td[0][3].appendChild(remove_software_agent);


tr[0] = table.insertRow(-1);
td[0] = [];
td[0][0] = tr[0].insertCell(-1);
td[0][1] = tr[0].insertCell(-1);
td[0][2] = tr[0].insertCell(-1);
td[0][3] = tr[0].insertCell(-1);
td[0][4] = tr[0].insertCell(-1);
td[0][5] = tr[0].insertCell(-1);

const all_entity  = document.createElement('input');
all_entity.setAttribute('type','submit');
all_entity.setAttribute('value','Every Entity');
all_entity.onclick = function(){window.location = '/all-entity.html'};

const insert_entity  = document.createElement('input');
insert_entity.setAttribute('type','submit');
insert_entity.setAttribute('value','Insert Entity');
insert_entity.onclick = function(){window.location = '/insert-entity.html'};

const remove_entity  = document.createElement('input');
remove_entity.setAttribute('type','submit');
remove_entity.setAttribute('value','Delete Entity');
remove_entity.onclick = function(){window.location = '/delete-entity.html'};


    td[0][0].appendChild(all_entity);
    td[0][1].appendChild(insert_entity);
    td[0][2].appendChild(remove_entity);


tr[0] = table.insertRow(-1);
td[0] = [];
td[0][0] = tr[0].insertCell(-1);
td[0][1] = tr[0].insertCell(-1);
td[0][2] = tr[0].insertCell(-1);
td[0][3] = tr[0].insertCell(-1);
td[0][4] = tr[0].insertCell(-1);
td[0][5] = tr[0].insertCell(-1);

const all_bundle  = document.createElement('input');
all_bundle.setAttribute('type','submit');
all_bundle.setAttribute('value','Every Bundle');
all_bundle.onclick = function(){window.location = '/all-bundle.html'};

const insert_bundle  = document.createElement('input');
insert_bundle.setAttribute('type','submit');
insert_bundle.setAttribute('value','Insert Bundle');
insert_bundle.onclick = function(){window.location = '/insert-bundle.html'};

const remove_bundle  = document.createElement('input');
remove_bundle.setAttribute('type','submit');
remove_bundle.setAttribute('value','Delete Bundle');
remove_bundle.onclick = function(){window.location = '/delete-bundle.html'};


const bundle_bent_arrow = document.createElement('img');
bundle_bent_arrow.src = 'bent.png'


td[0][0].appendChild(bundle_bent_arrow);
td[0][0].setAttribute('align', 'right');
td[0][1].appendChild(all_bundle);
td[0][2].appendChild(insert_bundle);
td[0][3].appendChild(remove_bundle);


tr[0] = table.insertRow(-1);
td[0] = [];
td[0][0] = tr[0].insertCell(-1);
td[0][1] = tr[0].insertCell(-1);
td[0][2] = tr[0].insertCell(-1);
td[0][3] = tr[0].insertCell(-1);
td[0][4] = tr[0].insertCell(-1);
td[0][5] = tr[0].insertCell(-1);

const all_collection  = document.createElement('input');
all_collection.setAttribute('type','submit');
all_collection.setAttribute('value','Every Collection');
all_collection.onclick = function(){window.location = '/all-collection.html'};

const insert_collection  = document.createElement('input');
insert_collection.setAttribute('type','submit');
insert_collection.setAttribute('value','Insert Collection');
insert_collection.onclick = function(){window.location = '/insert-collection.html'};

const remove_collection  = document.createElement('input');
remove_collection.setAttribute('type','submit');
remove_collection.setAttribute('value','Delete Collection');
remove_collection.onclick = function(){window.location = '/delete-collection.html'};


const collection_bent_arrow = document.createElement('img');
collection_bent_arrow.src = 'bent.png'


td[0][0].appendChild(collection_bent_arrow);
td[0][0].setAttribute('align', 'right');
td[0][1].appendChild(all_collection);
td[0][2].appendChild(insert_collection);
td[0][3].appendChild(remove_collection);


tr[0] = table.insertRow(-1);
td[0] = [];
td[0][0] = tr[0].insertCell(-1);
td[0][1] = tr[0].insertCell(-1);
td[0][2] = tr[0].insertCell(-1);
td[0][3] = tr[0].insertCell(-1);
td[0][4] = tr[0].insertCell(-1);
td[0][5] = tr[0].insertCell(-1);

const all_empty_collection  = document.createElement('input');
all_empty_collection.setAttribute('type','submit');
all_empty_collection.setAttribute('value','Every Empty Collection');
all_empty_collection.onclick = function(){window.location = '/all-empty_collection.html'};

const insert_empty_collection  = document.createElement('input');
insert_empty_collection.setAttribute('type','submit');
insert_empty_collection.setAttribute('value','Insert Empty Collection');
insert_empty_collection.onclick = function(){window.location = '/insert-empty_collection.html'};

const remove_empty_collection  = document.createElement('input');
remove_empty_collection.setAttribute('type','submit');
remove_empty_collection.setAttribute('value','Delete Empty Collection');
remove_empty_collection.onclick = function(){window.location = '/delete-empty_collection.html'};


const empty_collection_bent_arrow = document.createElement('img');
empty_collection_bent_arrow.src = 'bent.png'


const empty_collection_vertical = document.createElement('img');
empty_collection_vertical.src = 'vertical.png'
td[0][0].appendChild(empty_collection_vertical);
td[0][0].setAttribute('align', 'right');
td[0][1].appendChild(empty_collection_bent_arrow);
td[0][1].setAttribute('align', 'right');
td[0][2].appendChild(all_empty_collection);
td[0][3].appendChild(insert_empty_collection);
td[0][4].appendChild(remove_empty_collection);


tr[0] = table.insertRow(-1);
td[0] = [];
td[0][0] = tr[0].insertCell(-1);
td[0][1] = tr[0].insertCell(-1);
td[0][2] = tr[0].insertCell(-1);
td[0][3] = tr[0].insertCell(-1);
td[0][4] = tr[0].insertCell(-1);
td[0][5] = tr[0].insertCell(-1);

const all_plan  = document.createElement('input');
all_plan.setAttribute('type','submit');
all_plan.setAttribute('value','Every Plan');
all_plan.onclick = function(){window.location = '/all-plan.html'};

const insert_plan  = document.createElement('input');
insert_plan.setAttribute('type','submit');
insert_plan.setAttribute('value','Insert Plan');
insert_plan.onclick = function(){window.location = '/insert-plan.html'};

const remove_plan  = document.createElement('input');
remove_plan.setAttribute('type','submit');
remove_plan.setAttribute('value','Delete Plan');
remove_plan.onclick = function(){window.location = '/delete-plan.html'};


const plan_bent_arrow = document.createElement('img');
plan_bent_arrow.src = 'bent.png'


td[0][0].appendChild(plan_bent_arrow);
td[0][0].setAttribute('align', 'right');
td[0][1].appendChild(all_plan);
td[0][2].appendChild(insert_plan);
td[0][3].appendChild(remove_plan);


tr[0] = table.insertRow(-1);
td[0] = [];
td[0][0] = tr[0].insertCell(-1);
td[0][1] = tr[0].insertCell(-1);
td[0][2] = tr[0].insertCell(-1);
td[0][3] = tr[0].insertCell(-1);
td[0][4] = tr[0].insertCell(-1);
td[0][5] = tr[0].insertCell(-1);

const all_influence  = document.createElement('input');
all_influence.setAttribute('type','submit');
all_influence.setAttribute('value','Every Influence');
all_influence.onclick = function(){window.location = '/all-influence.html'};

const insert_influence  = document.createElement('input');
insert_influence.setAttribute('type','submit');
insert_influence.setAttribute('value','Insert Influence');
insert_influence.onclick = function(){window.location = '/insert-influence.html'};

const remove_influence  = document.createElement('input');
remove_influence.setAttribute('type','submit');
remove_influence.setAttribute('value','Delete Influence');
remove_influence.onclick = function(){window.location = '/delete-influence.html'};


    td[0][0].appendChild(all_influence);
    td[0][1].appendChild(insert_influence);
    td[0][2].appendChild(remove_influence);


tr[0] = table.insertRow(-1);
td[0] = [];
td[0][0] = tr[0].insertCell(-1);
td[0][1] = tr[0].insertCell(-1);
td[0][2] = tr[0].insertCell(-1);
td[0][3] = tr[0].insertCell(-1);
td[0][4] = tr[0].insertCell(-1);
td[0][5] = tr[0].insertCell(-1);

const all_activity_influence  = document.createElement('input');
all_activity_influence.setAttribute('type','submit');
all_activity_influence.setAttribute('value','Every Activity Influence');
all_activity_influence.onclick = function(){window.location = '/all-activity_influence.html'};

const insert_activity_influence  = document.createElement('input');
insert_activity_influence.setAttribute('type','submit');
insert_activity_influence.setAttribute('value','Insert Activity Influence');
insert_activity_influence.onclick = function(){window.location = '/insert-activity_influence.html'};

const remove_activity_influence  = document.createElement('input');
remove_activity_influence.setAttribute('type','submit');
remove_activity_influence.setAttribute('value','Delete Activity Influence');
remove_activity_influence.onclick = function(){window.location = '/delete-activity_influence.html'};


const activity_influence_bent_arrow = document.createElement('img');
activity_influence_bent_arrow.src = 'bent.png'


td[0][0].appendChild(activity_influence_bent_arrow);
td[0][0].setAttribute('align', 'right');
td[0][1].appendChild(all_activity_influence);
td[0][2].appendChild(insert_activity_influence);
td[0][3].appendChild(remove_activity_influence);


tr[0] = table.insertRow(-1);
td[0] = [];
td[0][0] = tr[0].insertCell(-1);
td[0][1] = tr[0].insertCell(-1);
td[0][2] = tr[0].insertCell(-1);
td[0][3] = tr[0].insertCell(-1);
td[0][4] = tr[0].insertCell(-1);
td[0][5] = tr[0].insertCell(-1);

const all_communication  = document.createElement('input');
all_communication.setAttribute('type','submit');
all_communication.setAttribute('value','Every Communication');
all_communication.onclick = function(){window.location = '/all-communication.html'};

const insert_communication  = document.createElement('input');
insert_communication.setAttribute('type','submit');
insert_communication.setAttribute('value','Insert Communication');
insert_communication.onclick = function(){window.location = '/insert-communication.html'};

const remove_communication  = document.createElement('input');
remove_communication.setAttribute('type','submit');
remove_communication.setAttribute('value','Delete Communication');
remove_communication.onclick = function(){window.location = '/delete-communication.html'};


const communication_bent_arrow = document.createElement('img');
communication_bent_arrow.src = 'bent.png'


const communication_vertical = document.createElement('img');
communication_vertical.src = 'vertical.png'
td[0][0].appendChild(communication_vertical);
td[0][0].setAttribute('align', 'right');
td[0][1].appendChild(communication_bent_arrow);
td[0][1].setAttribute('align', 'right');
td[0][2].appendChild(all_communication);
td[0][3].appendChild(insert_communication);
td[0][4].appendChild(remove_communication);


tr[0] = table.insertRow(-1);
td[0] = [];
td[0][0] = tr[0].insertCell(-1);
td[0][1] = tr[0].insertCell(-1);
td[0][2] = tr[0].insertCell(-1);
td[0][3] = tr[0].insertCell(-1);
td[0][4] = tr[0].insertCell(-1);
td[0][5] = tr[0].insertCell(-1);

const all_generation  = document.createElement('input');
all_generation.setAttribute('type','submit');
all_generation.setAttribute('value','Every Generation');
all_generation.onclick = function(){window.location = '/all-generation.html'};

const insert_generation  = document.createElement('input');
insert_generation.setAttribute('type','submit');
insert_generation.setAttribute('value','Insert Generation');
insert_generation.onclick = function(){window.location = '/insert-generation.html'};

const remove_generation  = document.createElement('input');
remove_generation.setAttribute('type','submit');
remove_generation.setAttribute('value','Delete Generation');
remove_generation.onclick = function(){window.location = '/delete-generation.html'};


const generation_bent_arrow = document.createElement('img');
generation_bent_arrow.src = 'bent.png'


const generation_vertical = document.createElement('img');
generation_vertical.src = 'vertical.png'
td[0][0].appendChild(generation_vertical);
td[0][0].setAttribute('align', 'right');
td[0][1].appendChild(generation_bent_arrow);
td[0][1].setAttribute('align', 'right');
td[0][2].appendChild(all_generation);
td[0][3].appendChild(insert_generation);
td[0][4].appendChild(remove_generation);


tr[0] = table.insertRow(-1);
td[0] = [];
td[0][0] = tr[0].insertCell(-1);
td[0][1] = tr[0].insertCell(-1);
td[0][2] = tr[0].insertCell(-1);
td[0][3] = tr[0].insertCell(-1);
td[0][4] = tr[0].insertCell(-1);
td[0][5] = tr[0].insertCell(-1);

const all_invalidation  = document.createElement('input');
all_invalidation.setAttribute('type','submit');
all_invalidation.setAttribute('value','Every Invalidation');
all_invalidation.onclick = function(){window.location = '/all-invalidation.html'};

const insert_invalidation  = document.createElement('input');
insert_invalidation.setAttribute('type','submit');
insert_invalidation.setAttribute('value','Insert Invalidation');
insert_invalidation.onclick = function(){window.location = '/insert-invalidation.html'};

const remove_invalidation  = document.createElement('input');
remove_invalidation.setAttribute('type','submit');
remove_invalidation.setAttribute('value','Delete Invalidation');
remove_invalidation.onclick = function(){window.location = '/delete-invalidation.html'};


const invalidation_bent_arrow = document.createElement('img');
invalidation_bent_arrow.src = 'bent.png'


const invalidation_vertical = document.createElement('img');
invalidation_vertical.src = 'vertical.png'
td[0][0].appendChild(invalidation_vertical);
td[0][0].setAttribute('align', 'right');
td[0][1].appendChild(invalidation_bent_arrow);
td[0][1].setAttribute('align', 'right');
td[0][2].appendChild(all_invalidation);
td[0][3].appendChild(insert_invalidation);
td[0][4].appendChild(remove_invalidation);


tr[0] = table.insertRow(-1);
td[0] = [];
td[0][0] = tr[0].insertCell(-1);
td[0][1] = tr[0].insertCell(-1);
td[0][2] = tr[0].insertCell(-1);
td[0][3] = tr[0].insertCell(-1);
td[0][4] = tr[0].insertCell(-1);
td[0][5] = tr[0].insertCell(-1);

const all_agent_influence  = document.createElement('input');
all_agent_influence.setAttribute('type','submit');
all_agent_influence.setAttribute('value','Every Agent Influence');
all_agent_influence.onclick = function(){window.location = '/all-agent_influence.html'};

const insert_agent_influence  = document.createElement('input');
insert_agent_influence.setAttribute('type','submit');
insert_agent_influence.setAttribute('value','Insert Agent Influence');
insert_agent_influence.onclick = function(){window.location = '/insert-agent_influence.html'};

const remove_agent_influence  = document.createElement('input');
remove_agent_influence.setAttribute('type','submit');
remove_agent_influence.setAttribute('value','Delete Agent Influence');
remove_agent_influence.onclick = function(){window.location = '/delete-agent_influence.html'};


const agent_influence_bent_arrow = document.createElement('img');
agent_influence_bent_arrow.src = 'bent.png'


td[0][0].appendChild(agent_influence_bent_arrow);
td[0][0].setAttribute('align', 'right');
td[0][1].appendChild(all_agent_influence);
td[0][2].appendChild(insert_agent_influence);
td[0][3].appendChild(remove_agent_influence);


tr[0] = table.insertRow(-1);
td[0] = [];
td[0][0] = tr[0].insertCell(-1);
td[0][1] = tr[0].insertCell(-1);
td[0][2] = tr[0].insertCell(-1);
td[0][3] = tr[0].insertCell(-1);
td[0][4] = tr[0].insertCell(-1);
td[0][5] = tr[0].insertCell(-1);

const all_association  = document.createElement('input');
all_association.setAttribute('type','submit');
all_association.setAttribute('value','Every Association');
all_association.onclick = function(){window.location = '/all-association.html'};

const insert_association  = document.createElement('input');
insert_association.setAttribute('type','submit');
insert_association.setAttribute('value','Insert Association');
insert_association.onclick = function(){window.location = '/insert-association.html'};

const remove_association  = document.createElement('input');
remove_association.setAttribute('type','submit');
remove_association.setAttribute('value','Delete Association');
remove_association.onclick = function(){window.location = '/delete-association.html'};


const association_bent_arrow = document.createElement('img');
association_bent_arrow.src = 'bent.png'


const association_vertical = document.createElement('img');
association_vertical.src = 'vertical.png'
td[0][0].appendChild(association_vertical);
td[0][0].setAttribute('align', 'right');
td[0][1].appendChild(association_bent_arrow);
td[0][1].setAttribute('align', 'right');
td[0][2].appendChild(all_association);
td[0][3].appendChild(insert_association);
td[0][4].appendChild(remove_association);


tr[0] = table.insertRow(-1);
td[0] = [];
td[0][0] = tr[0].insertCell(-1);
td[0][1] = tr[0].insertCell(-1);
td[0][2] = tr[0].insertCell(-1);
td[0][3] = tr[0].insertCell(-1);
td[0][4] = tr[0].insertCell(-1);
td[0][5] = tr[0].insertCell(-1);

const all_attribution  = document.createElement('input');
all_attribution.setAttribute('type','submit');
all_attribution.setAttribute('value','Every Attribution');
all_attribution.onclick = function(){window.location = '/all-attribution.html'};

const insert_attribution  = document.createElement('input');
insert_attribution.setAttribute('type','submit');
insert_attribution.setAttribute('value','Insert Attribution');
insert_attribution.onclick = function(){window.location = '/insert-attribution.html'};

const remove_attribution  = document.createElement('input');
remove_attribution.setAttribute('type','submit');
remove_attribution.setAttribute('value','Delete Attribution');
remove_attribution.onclick = function(){window.location = '/delete-attribution.html'};


const attribution_bent_arrow = document.createElement('img');
attribution_bent_arrow.src = 'bent.png'


const attribution_vertical = document.createElement('img');
attribution_vertical.src = 'vertical.png'
td[0][0].appendChild(attribution_vertical);
td[0][0].setAttribute('align', 'right');
td[0][1].appendChild(attribution_bent_arrow);
td[0][1].setAttribute('align', 'right');
td[0][2].appendChild(all_attribution);
td[0][3].appendChild(insert_attribution);
td[0][4].appendChild(remove_attribution);


tr[0] = table.insertRow(-1);
td[0] = [];
td[0][0] = tr[0].insertCell(-1);
td[0][1] = tr[0].insertCell(-1);
td[0][2] = tr[0].insertCell(-1);
td[0][3] = tr[0].insertCell(-1);
td[0][4] = tr[0].insertCell(-1);
td[0][5] = tr[0].insertCell(-1);

const all_delegation  = document.createElement('input');
all_delegation.setAttribute('type','submit');
all_delegation.setAttribute('value','Every Delegation');
all_delegation.onclick = function(){window.location = '/all-delegation.html'};

const insert_delegation  = document.createElement('input');
insert_delegation.setAttribute('type','submit');
insert_delegation.setAttribute('value','Insert Delegation');
insert_delegation.onclick = function(){window.location = '/insert-delegation.html'};

const remove_delegation  = document.createElement('input');
remove_delegation.setAttribute('type','submit');
remove_delegation.setAttribute('value','Delete Delegation');
remove_delegation.onclick = function(){window.location = '/delete-delegation.html'};


const delegation_bent_arrow = document.createElement('img');
delegation_bent_arrow.src = 'bent.png'


const delegation_vertical = document.createElement('img');
delegation_vertical.src = 'vertical.png'
td[0][0].appendChild(delegation_vertical);
td[0][0].setAttribute('align', 'right');
td[0][1].appendChild(delegation_bent_arrow);
td[0][1].setAttribute('align', 'right');
td[0][2].appendChild(all_delegation);
td[0][3].appendChild(insert_delegation);
td[0][4].appendChild(remove_delegation);


tr[0] = table.insertRow(-1);
td[0] = [];
td[0][0] = tr[0].insertCell(-1);
td[0][1] = tr[0].insertCell(-1);
td[0][2] = tr[0].insertCell(-1);
td[0][3] = tr[0].insertCell(-1);
td[0][4] = tr[0].insertCell(-1);
td[0][5] = tr[0].insertCell(-1);

const all_entity_influence  = document.createElement('input');
all_entity_influence.setAttribute('type','submit');
all_entity_influence.setAttribute('value','Every Entity Influence');
all_entity_influence.onclick = function(){window.location = '/all-entity_influence.html'};

const insert_entity_influence  = document.createElement('input');
insert_entity_influence.setAttribute('type','submit');
insert_entity_influence.setAttribute('value','Insert Entity Influence');
insert_entity_influence.onclick = function(){window.location = '/insert-entity_influence.html'};

const remove_entity_influence  = document.createElement('input');
remove_entity_influence.setAttribute('type','submit');
remove_entity_influence.setAttribute('value','Delete Entity Influence');
remove_entity_influence.onclick = function(){window.location = '/delete-entity_influence.html'};


const entity_influence_bent_arrow = document.createElement('img');
entity_influence_bent_arrow.src = 'bent.png'


td[0][0].appendChild(entity_influence_bent_arrow);
td[0][0].setAttribute('align', 'right');
td[0][1].appendChild(all_entity_influence);
td[0][2].appendChild(insert_entity_influence);
td[0][3].appendChild(remove_entity_influence);


tr[0] = table.insertRow(-1);
td[0] = [];
td[0][0] = tr[0].insertCell(-1);
td[0][1] = tr[0].insertCell(-1);
td[0][2] = tr[0].insertCell(-1);
td[0][3] = tr[0].insertCell(-1);
td[0][4] = tr[0].insertCell(-1);
td[0][5] = tr[0].insertCell(-1);

const all_derivation  = document.createElement('input');
all_derivation.setAttribute('type','submit');
all_derivation.setAttribute('value','Every Derivation');
all_derivation.onclick = function(){window.location = '/all-derivation.html'};

const insert_derivation  = document.createElement('input');
insert_derivation.setAttribute('type','submit');
insert_derivation.setAttribute('value','Insert Derivation');
insert_derivation.onclick = function(){window.location = '/insert-derivation.html'};

const remove_derivation  = document.createElement('input');
remove_derivation.setAttribute('type','submit');
remove_derivation.setAttribute('value','Delete Derivation');
remove_derivation.onclick = function(){window.location = '/delete-derivation.html'};


const derivation_bent_arrow = document.createElement('img');
derivation_bent_arrow.src = 'bent.png'


const derivation_vertical = document.createElement('img');
derivation_vertical.src = 'vertical.png'
td[0][0].innerHTML = '</nbsp>';
td[0][1].appendChild(derivation_bent_arrow);
td[0][1].setAttribute('align', 'right');
td[0][2].appendChild(all_derivation);
td[0][3].appendChild(insert_derivation);
td[0][4].appendChild(remove_derivation);


tr[0] = table.insertRow(-1);
td[0] = [];
td[0][0] = tr[0].insertCell(-1);
td[0][1] = tr[0].insertCell(-1);
td[0][2] = tr[0].insertCell(-1);
td[0][3] = tr[0].insertCell(-1);
td[0][4] = tr[0].insertCell(-1);
td[0][5] = tr[0].insertCell(-1);

const all_primary_source  = document.createElement('input');
all_primary_source.setAttribute('type','submit');
all_primary_source.setAttribute('value','Every Primary Source');
all_primary_source.onclick = function(){window.location = '/all-primary_source.html'};

const insert_primary_source  = document.createElement('input');
insert_primary_source.setAttribute('type','submit');
insert_primary_source.setAttribute('value','Insert Primary Source');
insert_primary_source.onclick = function(){window.location = '/insert-primary_source.html'};

const remove_primary_source  = document.createElement('input');
remove_primary_source.setAttribute('type','submit');
remove_primary_source.setAttribute('value','Delete Primary Source');
remove_primary_source.onclick = function(){window.location = '/delete-primary_source.html'};


const primary_source_bent_arrow = document.createElement('img');
primary_source_bent_arrow.src = 'bent.png'


td[0][0].innerHTML = '<\/nbsp>';
const primary_source_vertical_other = document.createElement('img');
primary_source_vertical_other.src = 'vertical.png'
td[0][1].appendChild(primary_source_vertical_other);
td[0][1].setAttribute('align', 'right');
td[0][2].appendChild(primary_source_bent_arrow);
td[0][2].setAttribute('align', 'right');
td[0][3].appendChild(all_primary_source);
td[0][4].appendChild(insert_primary_source);
td[0][5].appendChild(remove_primary_source);


tr[0] = table.insertRow(-1);
td[0] = [];
td[0][0] = tr[0].insertCell(-1);
td[0][1] = tr[0].insertCell(-1);
td[0][2] = tr[0].insertCell(-1);
td[0][3] = tr[0].insertCell(-1);
td[0][4] = tr[0].insertCell(-1);
td[0][5] = tr[0].insertCell(-1);

const all_quotation  = document.createElement('input');
all_quotation.setAttribute('type','submit');
all_quotation.setAttribute('value','Every Quotation');
all_quotation.onclick = function(){window.location = '/all-quotation.html'};

const insert_quotation  = document.createElement('input');
insert_quotation.setAttribute('type','submit');
insert_quotation.setAttribute('value','Insert Quotation');
insert_quotation.onclick = function(){window.location = '/insert-quotation.html'};

const remove_quotation  = document.createElement('input');
remove_quotation.setAttribute('type','submit');
remove_quotation.setAttribute('value','Delete Quotation');
remove_quotation.onclick = function(){window.location = '/delete-quotation.html'};


const quotation_bent_arrow = document.createElement('img');
quotation_bent_arrow.src = 'bent.png'


td[0][0].innerHTML = '<\/nbsp>';
const quotation_vertical_other = document.createElement('img');
quotation_vertical_other.src = 'vertical.png'
td[0][1].appendChild(quotation_vertical_other);
td[0][1].setAttribute('align', 'right');
td[0][2].appendChild(quotation_bent_arrow);
td[0][2].setAttribute('align', 'right');
td[0][3].appendChild(all_quotation);
td[0][4].appendChild(insert_quotation);
td[0][5].appendChild(remove_quotation);


tr[0] = table.insertRow(-1);
td[0] = [];
td[0][0] = tr[0].insertCell(-1);
td[0][1] = tr[0].insertCell(-1);
td[0][2] = tr[0].insertCell(-1);
td[0][3] = tr[0].insertCell(-1);
td[0][4] = tr[0].insertCell(-1);
td[0][5] = tr[0].insertCell(-1);

const all_revision  = document.createElement('input');
all_revision.setAttribute('type','submit');
all_revision.setAttribute('value','Every Revision');
all_revision.onclick = function(){window.location = '/all-revision.html'};

const insert_revision  = document.createElement('input');
insert_revision.setAttribute('type','submit');
insert_revision.setAttribute('value','Insert Revision');
insert_revision.onclick = function(){window.location = '/insert-revision.html'};

const remove_revision  = document.createElement('input');
remove_revision.setAttribute('type','submit');
remove_revision.setAttribute('value','Delete Revision');
remove_revision.onclick = function(){window.location = '/delete-revision.html'};


const revision_bent_arrow = document.createElement('img');
revision_bent_arrow.src = 'bent.png'


td[0][0].innerHTML = '<\/nbsp>';
const revision_vertical_other = document.createElement('img');
revision_vertical_other.src = 'vertical.png'
td[0][1].appendChild(revision_vertical_other);
td[0][1].setAttribute('align', 'right');
td[0][2].appendChild(revision_bent_arrow);
td[0][2].setAttribute('align', 'right');
td[0][3].appendChild(all_revision);
td[0][4].appendChild(insert_revision);
td[0][5].appendChild(remove_revision);


tr[0] = table.insertRow(-1);
td[0] = [];
td[0][0] = tr[0].insertCell(-1);
td[0][1] = tr[0].insertCell(-1);
td[0][2] = tr[0].insertCell(-1);
td[0][3] = tr[0].insertCell(-1);
td[0][4] = tr[0].insertCell(-1);
td[0][5] = tr[0].insertCell(-1);

const all_ends  = document.createElement('input');
all_ends.setAttribute('type','submit');
all_ends.setAttribute('value','Every Ends');
all_ends.onclick = function(){window.location = '/all-ends.html'};

const insert_ends  = document.createElement('input');
insert_ends.setAttribute('type','submit');
insert_ends.setAttribute('value','Insert Ends');
insert_ends.onclick = function(){window.location = '/insert-ends.html'};

const remove_ends  = document.createElement('input');
remove_ends.setAttribute('type','submit');
remove_ends.setAttribute('value','Delete Ends');
remove_ends.onclick = function(){window.location = '/delete-ends.html'};


const ends_bent_arrow = document.createElement('img');
ends_bent_arrow.src = 'bent.png'


const ends_vertical = document.createElement('img');
ends_vertical.src = 'vertical.png'
td[0][0].innerHTML = '</nbsp>';
td[0][1].appendChild(ends_bent_arrow);
td[0][1].setAttribute('align', 'right');
td[0][2].appendChild(all_ends);
td[0][3].appendChild(insert_ends);
td[0][4].appendChild(remove_ends);


tr[0] = table.insertRow(-1);
td[0] = [];
td[0][0] = tr[0].insertCell(-1);
td[0][1] = tr[0].insertCell(-1);
td[0][2] = tr[0].insertCell(-1);
td[0][3] = tr[0].insertCell(-1);
td[0][4] = tr[0].insertCell(-1);
td[0][5] = tr[0].insertCell(-1);

const all_start  = document.createElement('input');
all_start.setAttribute('type','submit');
all_start.setAttribute('value','Every Start');
all_start.onclick = function(){window.location = '/all-start.html'};

const insert_start  = document.createElement('input');
insert_start.setAttribute('type','submit');
insert_start.setAttribute('value','Insert Start');
insert_start.onclick = function(){window.location = '/insert-start.html'};

const remove_start  = document.createElement('input');
remove_start.setAttribute('type','submit');
remove_start.setAttribute('value','Delete Start');
remove_start.onclick = function(){window.location = '/delete-start.html'};


const start_bent_arrow = document.createElement('img');
start_bent_arrow.src = 'bent.png'


const start_vertical = document.createElement('img');
start_vertical.src = 'vertical.png'
td[0][0].innerHTML = '</nbsp>';
td[0][1].appendChild(start_bent_arrow);
td[0][1].setAttribute('align', 'right');
td[0][2].appendChild(all_start);
td[0][3].appendChild(insert_start);
td[0][4].appendChild(remove_start);


tr[0] = table.insertRow(-1);
td[0] = [];
td[0][0] = tr[0].insertCell(-1);
td[0][1] = tr[0].insertCell(-1);
td[0][2] = tr[0].insertCell(-1);
td[0][3] = tr[0].insertCell(-1);
td[0][4] = tr[0].insertCell(-1);
td[0][5] = tr[0].insertCell(-1);

const all_usage  = document.createElement('input');
all_usage.setAttribute('type','submit');
all_usage.setAttribute('value','Every Usage');
all_usage.onclick = function(){window.location = '/all-usage.html'};

const insert_usage  = document.createElement('input');
insert_usage.setAttribute('type','submit');
insert_usage.setAttribute('value','Insert Usage');
insert_usage.onclick = function(){window.location = '/insert-usage.html'};

const remove_usage  = document.createElement('input');
remove_usage.setAttribute('type','submit');
remove_usage.setAttribute('value','Delete Usage');
remove_usage.onclick = function(){window.location = '/delete-usage.html'};


const usage_bent_arrow = document.createElement('img');
usage_bent_arrow.src = 'bent.png'


const usage_vertical = document.createElement('img');
usage_vertical.src = 'vertical.png'
td[0][0].innerHTML = '</nbsp>';
td[0][1].appendChild(usage_bent_arrow);
td[0][1].setAttribute('align', 'right');
td[0][2].appendChild(all_usage);
td[0][3].appendChild(insert_usage);
td[0][4].appendChild(remove_usage);


tr[0] = table.insertRow(-1);
td[0] = [];
td[0][0] = tr[0].insertCell(-1);
td[0][1] = tr[0].insertCell(-1);
td[0][2] = tr[0].insertCell(-1);
td[0][3] = tr[0].insertCell(-1);
td[0][4] = tr[0].insertCell(-1);
td[0][5] = tr[0].insertCell(-1);

const all_instantaneous_event  = document.createElement('input');
all_instantaneous_event.setAttribute('type','submit');
all_instantaneous_event.setAttribute('value','Every Instantaneous Event');
all_instantaneous_event.onclick = function(){window.location = '/all-instantaneous_event.html'};

const insert_instantaneous_event  = document.createElement('input');
insert_instantaneous_event.setAttribute('type','submit');
insert_instantaneous_event.setAttribute('value','Insert Instantaneous Event');
insert_instantaneous_event.onclick = function(){window.location = '/insert-instantaneous_event.html'};

const remove_instantaneous_event  = document.createElement('input');
remove_instantaneous_event.setAttribute('type','submit');
remove_instantaneous_event.setAttribute('value','Delete Instantaneous Event');
remove_instantaneous_event.onclick = function(){window.location = '/delete-instantaneous_event.html'};


    td[0][0].appendChild(all_instantaneous_event);
    td[0][1].appendChild(insert_instantaneous_event);
    td[0][2].appendChild(remove_instantaneous_event);


tr[0] = table.insertRow(-1);
td[0] = [];
td[0][0] = tr[0].insertCell(-1);
td[0][1] = tr[0].insertCell(-1);
td[0][2] = tr[0].insertCell(-1);
td[0][3] = tr[0].insertCell(-1);
td[0][4] = tr[0].insertCell(-1);
td[0][5] = tr[0].insertCell(-1);

const all_location  = document.createElement('input');
all_location.setAttribute('type','submit');
all_location.setAttribute('value','Every Location');
all_location.onclick = function(){window.location = '/all-location.html'};

const insert_location  = document.createElement('input');
insert_location.setAttribute('type','submit');
insert_location.setAttribute('value','Insert Location');
insert_location.onclick = function(){window.location = '/insert-location.html'};

const remove_location  = document.createElement('input');
remove_location.setAttribute('type','submit');
remove_location.setAttribute('value','Delete Location');
remove_location.onclick = function(){window.location = '/delete-location.html'};


    td[0][0].appendChild(all_location);
    td[0][1].appendChild(insert_location);
    td[0][2].appendChild(remove_location);


tr[0] = table.insertRow(-1);
td[0] = [];
td[0][0] = tr[0].insertCell(-1);
td[0][1] = tr[0].insertCell(-1);
td[0][2] = tr[0].insertCell(-1);
td[0][3] = tr[0].insertCell(-1);
td[0][4] = tr[0].insertCell(-1);
td[0][5] = tr[0].insertCell(-1);

const all_role  = document.createElement('input');
all_role.setAttribute('type','submit');
all_role.setAttribute('value','Every Role');
all_role.onclick = function(){window.location = '/all-role.html'};

const insert_role  = document.createElement('input');
insert_role.setAttribute('type','submit');
insert_role.setAttribute('value','Insert Role');
insert_role.onclick = function(){window.location = '/insert-role.html'};

const remove_role  = document.createElement('input');
remove_role.setAttribute('type','submit');
remove_role.setAttribute('value','Delete Role');
remove_role.onclick = function(){window.location = '/delete-role.html'};


    td[0][0].appendChild(all_role);
    td[0][1].appendChild(insert_role);
    td[0][2].appendChild(remove_role);

