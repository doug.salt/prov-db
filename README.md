![logo](https://local-social-innovation.eu/fileadmin/templates/code/images/logos/logo.svg)

# PURPOSE

This is an attempt to create to operationalize the
[PROV-O](https://www.w3.org/TR/prov-o/) provenance ontology. This is part of an
ongoing attempt to rationalise the data for Horizon 2020 project,
[SMARTEES](https://local-social-innovation.eu/). The primary aim is to create a
data that is suitable for inputs into the agent-based models that are to used
in the [SMARTEES](https://local-social-innovation.eu/) project.

For data to be usuable it should be FAIR [@wilkinson_et_al_2016]:

1. **F**indable
2. **A**ccessable
3. **I**nteroperable
4. **R**eusable

The provenance database addresses aspects of all of these. The provenance
ontology tracks where the data is (**F**indable), explains how the data was
formed (**A**ccessable and **I**nteroperable), which should lead to the data
being **R**eusuable. 

In this first version of the database, we are concentrating on provenance.
Eventually the database will be extended with formalised annotations for
metadata. These annotation will describe internal structure, assumptions, and
obviously schema and data types.

# VERSION

Alpha

# INSTALL

## Slackware (well, actually Salix)

```bash
slapt-get -i httpd
chmod +x /etc/rc.d/rc.httpd
ln -s (git home dir)/prov-db/htdocs
/etc/rc.d/rc.httpd start
```

## In general

```bash
pip3 install flask
pip3 install flask_restful.
```

# CONTACT

dougDOTsaltAThuttonDOTacDOTuk

# MANIFEST

+ LICENSE - the license for this software
+ README.md - this file
+ htdocs - the directory with the reference implementation for the front-end
+ lib.bib - a list of literature referred to in the README.md
+ logo.png - SMARTEES logo and the logo for this repository.
+ provo.py - the webservice application python implementation.
+ provo.sh - the simple script required to run up the flask implementation, provo.py.
+ sql - the sql required to create the database.
