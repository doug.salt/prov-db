CREATE TABLE IF NOT EXISTS metadata_language (
    NAME TEXT PRIMARY KEY
);

INSERT INTO metadata_language (NAME) VALUES ('en');
INSERT INTO metadata_language (NAME) VALUES ('en_gb');


CREATE TABLE IF NOT EXISTS metadata_object_type (
    NAME TEXT PRIMARY KEY
);

INSERT INTO metadata_object_type (NAME) VALUES ('table');
INSERT INTO metadata_object_type (NAME) VALUES ('row');
INSERT INTO metadata_object_type (NAME) VALUES ('column');
INSERT INTO metadata_object_type (NAME) VALUES ('field');
INSERT INTO metadata_object_type (NAME) VALUES ('xsd:anyURI');

CREATE TABLE IF NOT EXISTS table_metadata (
    TABLE_NAME TEXT,
    PREDICATE TEXT,
    OBJECT TEXT, 
    OBJECT_TYPE TEXT,
    LANGUAGE TEXT,
    PRIMARY KEY (TABLE_NAME, PREDICATE, OBJECT),
    FOREIGN KEY (OBJECT_TYPE) REFERENCES metadata_object_type(NAME),
    FOREIGN KEY (LANGUAGE) REFERENCES metadata_language(NAME)
);

-- In OWL terms this is the datatype or relationship being annotated

CREATE TABLE IF NOT EXISTS column_metadata (
    TABLE_NAME TEXT,
    ROW_NAME TEXT,
    PREDICATE TEXT,
    OBJECT TEXT, 
    OBJECT_TYPE TEXT,
    LANGUAGE TEXT,
    PRIMARY KEY (TABLE_NAME, ROW_NAME, PREDICATE, OBJECT),
    FOREIGN KEY (OBJECT_TYPE) REFERENCES metadata_object_type(NAME),
    FOREIGN KEY (LANGUAGE) REFERENCES metadata_language(NAME)
);

-- In OWL terms this is the instance

CREATE TABLE IF NOT EXISTS row_metadata (
    TABLE_NAME TEXT,
    PRIMARY_KEY TEXT, -- This will be the primary key of the row
    PREDICATE TEXT,
    OBJECT TEXT, 
    OBJECT_TYPE TEXT,
    LANGUAGE TEXT,
    PRIMARY KEY (TABLE_NAME, PRIMARY_KEY, PREDICATE, OBJECT),
    FOREIGN KEY (OBJECT_TYPE) REFERENCES metadata_object_type(NAME),
    FOREIGN KEY (LANGUAGE) REFERENCES metadata_language(NAME)
);

-- In OWL terms this is the value for a datatype linked to an instance

CREATE TABLE IF NOT EXISTS field_metadata (
    TABLE_NAME TEXT,
    PRIMARY_KEY TEXT, -- This will be the primary key of the row
    ROW_NAME TEXT,
    PREDICATE TEXT,
    OBJECT TEXT, 
    OBJECT_TYPE TEXT,
    LANGUAGE TEXT,
    PRIMARY KEY (TABLE_NAME, PRIMARY_KEY, ROW_NAME, PREDICATE, OBJECT),
    FOREIGN KEY (OBJECT_TYPE) REFERENCES metadata_object_type(NAME),
    FOREIGN KEY (LANGUAGE) REFERENCES metadata_language(NAME)
);

-- We can do later integrity checking by makeing TABLE_NAME, OBJECT and
-- PREDICATE foreign keys in other tables with TABLE_NAME, OBJECT and PREDICATE
-- as primary keys. This will also act as a look up table. I will do this
-- later.

-- ======================================================================

CREATE TABLE IF NOT EXISTS activity (
    NAME TEXT PRIMARY KEY,
    ENDED_AT_TIME TIMESTAMP,
    STARTED_AT_TIME TIMESTAMP,
    AT_LOCATION TEXT,
    GENERATED TEXT,
    INVALIDATED TEXT,
    QUALFIED_ASSOCIATION TEXT,
    QUALFIED_COMMUNICATION TEXT,
    QUALFIED_END TEXT,
    QUALFIED_INFLUENCE TEXT,
    QUALFIED_START TEXT,
    QUALFIED_USAGE TEXT,
    WAS_ASSOCIATED_WITH TEXT,
    WAS_ENDED_BY TEXT,
    WAS_GENERATED_BY TEXT,
    WAS_INFLUENCED_BY_ACTIVITY TEXT,
    WAS_INFLUENCED_BY_AGENT TEXT,
    WAS_INFLUENCED_BY_ENTITY TEXT,
    WAS_INFORMED_BY TEXT,
    WAS_STARTED_BY TEXT
);

INSERT INTO table_metadata (TABLE_NAME, PREDICATE, OBJECT) 
    VALUES ('activity', 'rdfs:label','Activity');
INSERT INTO table_metadata (TABLE_NAME, PREDICATE, OBJECT) 
    VALUES ('activity', 'rdfs:isDefinedBy','http://www.w3.org/ns/prov-o#');
INSERT INTO table_metadata (TABLE_NAME, PREDICATE, OBJECT) 
    VALUES ('activity', 'prov:category','starting-point');
INSERT INTO table_metadata (TABLE_NAME, PREDICATE, OBJECT) 
    VALUES ('activity', 'prov:component','entities-activities');
INSERT INTO table_metadata (TABLE_NAME, PREDICATE, OBJECT) 
    VALUES ('activity', 'prov:constraints','http://www.w3.org/TR/2013/REC-prov-constraints-20130430/#prov-dm-constraints-fig');
INSERT INTO table_metadata (TABLE_NAME, PREDICATE, OBJECT) 
    VALUES ('activity', 'prov:definition','An activity is something that occurs over a period of time and acts upon or with entities; it may include consuming, processing, transforming, modifying, relocating, using, or generating entities.');
INSERT INTO table_metadata (TABLE_NAME, PREDICATE, OBJECT) 
    VALUES ('activity', 'prov:dm','http://www.w3.org/TR/2013/REC-prov-dm-20130430/#term-activity');
INSERT INTO table_metadata (TABLE_NAME, PREDICATE, OBJECT) 
    VALUES ('activity', 'prov:n','http://www.w3.org/TR/2013/REC-prov-n-20130430/#expression-activity');


INSERT INTO column_metadata (TABLE_NAME, ROW_NAME, PREDICATE, OBJECT, OBJECT_TYPE) 
    VALUES ('influence', 'hadActivity', 'prov:sharesDefinitionWith','activity','table');

-- ======================================================================

CREATE TABLE IF NOT EXISTS agent (
    NAME TEXT PRIMARY KEY,
    AT_LOCATION TEXT,
    QUALIFIED_DELEGATION TEXT,
    QUALIFIED_INFLUENCE TEXT,
    ACTED_ON_BEHALF_OF TEXT,
    WAS_INFLUENCED_BY_ACTIVITY TEXT,
    WAS_INFLUENCED_BY_AGENT TEXT,
    WAS_INFLUENCED_BY_ENTITY TEXT
);

INSERT INTO table_metadata (TABLE_NAME, PREDICATE, OBJECT) 
    VALUES ('agent', 'rdfs:label','Agent');
INSERT INTO table_metadata (TABLE_NAME, PREDICATE, OBJECT) 
    VALUES ('agent', 'rdfs:isDefinedBy','http://www.w3.org/ns/prov-o#');
INSERT INTO table_metadata (TABLE_NAME, PREDICATE, OBJECT) 
    VALUES ('agent', 'prov:category','starting-point');
INSERT INTO table_metadata (TABLE_NAME, PREDICATE, OBJECT) 
    VALUES ('agent', 'prov:component','agents-responsibility');
INSERT INTO table_metadata (TABLE_NAME, PREDICATE, OBJECT) 
    VALUES ('agent', 'prov:constraints','http://www.w3.org/TR/2013/REC-prov-constraints-20130430/#prov-dm-constraints-fig');
INSERT INTO table_metadata (TABLE_NAME, PREDICATE, OBJECT) 
    VALUES ('agent', 'prov:definition','An agent is something that bears some form of responsibility for an activity taking place, for the existence of an entity, or for another agent''s activity.');
INSERT INTO table_metadata (TABLE_NAME, PREDICATE, OBJECT) 
    VALUES ('agent', 'prov:dm','http://www.w3.org/TR/2013/REC-prov-dm-20130430/#term-agent');
INSERT INTO table_metadata (TABLE_NAME, PREDICATE, OBJECT) 
    VALUES ('agent', 'prov:n','http://www.w3.org/TR/2013/REC-prov-n-20130430/#expression-agent');

-- ======================================================================

CREATE TABLE IF NOT EXISTS organization (
    NAME TEXT PRIMARY KEY,
    AT_LOCATION TEXT,
    QUALIFIED_DELEGATION TEXT,
    QUALIFIED_INFLUENCE TEXT,
    ACTED_ON_BEHALF_OF TEXT,
    WAS_INFLUENCED_BY_ACTIVITY TEXT,
    WAS_INFLUENCED_BY_AGENT TEXT,
    WAS_INFLUENCED_BY_ENTITY TEXT
);

INSERT INTO table_metadata (TABLE_NAME, PREDICATE, OBJECT) 
    VALUES ('organization', 'rdfs:subClassOf', 'agent');

-- ======================================================================

CREATE TABLE IF NOT EXISTS person (
    NAME TEXT PRIMARY KEY,
    AT_LOCATION TEXT,
    QUALIFIED_DELEGATION TEXT,
    QUALIFIED_INFLUENCE TEXT,
    ACTED_ON_BEHALF_OF TEXT,
    WAS_INFLUENCED_BY_ACTIVITY TEXT,
    WAS_INFLUENCED_BY_AGENT TEXT,
    WAS_INFLUENCED_BY_ENTITY TEXT
);

INSERT INTO table_metadata (TABLE_NAME, PREDICATE, OBJECT) 
    VALUES ('person', 'rdfs:label','Person');

INSERT INTO table_metadata (TABLE_NAME, PREDICATE, OBJECT) 
    VALUES ('person', 'rdfs:subClassOf', 'agent');

-- ======================================================================

CREATE TABLE IF NOT EXISTS software_agent (
    NAME TEXT PRIMARY KEY,
    AT_LOCATION TEXT,
    QUALIFIED_DELEGATION TEXT,
    QUALIFIED_INFLUENCE TEXT,
    ACTED_ON_BEHALF_OF TEXT,
    WAS_INFLUENCED_BY_ACTIVITY TEXT,
    WAS_INFLUENCED_BY_AGENT TEXT,
    WAS_INFLUENCED_BY_ENTITY TEXT
);

INSERT INTO table_metadata (TABLE_NAME, PREDICATE, OBJECT) 
    VALUES ('software_agent', 'rdfs:label','SoftwareAgent');

INSERT INTO table_metadata (TABLE_NAME, PREDICATE, OBJECT) 
    VALUES ('software_agent', 'rdfs:subClassOf', 'agent');

-- ======================================================================

CREATE TABLE IF NOT EXISTS entity (
    NAME TEXT PRIMARY KEY,
    GENERATED_AT_TIME DATE,
    INVALIDATED_AT_TIME DATE,
    AT_LOCATION TEXT,
    HAD_PRIMARY_SOURCE TEXT,
    ALTERNATE_OF TEXT,
    QUALIFIED_ATTRIBUTION TEXT,
    QUALIFIED_DERIVATION TEXT,
    QUALIFIED_GENERATION TEXT,
    QUALIFIED_INFLUENCE TEXT,
    QUALIFIED_INVALIDATION TEXT,
    QUALIFIED_PRIMARY_SOURCE TEXT,
    QUALIFIED_QUOTATION TEXT,
    QUALIFIED_REVISION TEXT,
    SPECIALIZATION_OF TEXT,
    HAS_VALUE TEXT,
    WAS_ATTRIBUTED_TO TEXT,
    WAS_DERIVED_FROM TEXT,
    WAS_GENERATED_BY TEXT,
    WAS_INFLUENCED_BY_ACTIVITY TEXT,
    WAS_INFLUENCED_BY_AGENT TEXT,
    WAS_INFLUENCED_BY_ENTITY TEXT,
    WAS_INVALIDATED_BY TEXT,
    WAS_QUOTED_FROM TEXT

);

INSERT INTO table_metadata (TABLE_NAME, PREDICATE, OBJECT) 
    VALUES ('entity', 'rdfs:label','Entity');
INSERT INTO table_metadata (TABLE_NAME, PREDICATE, OBJECT) 
    VALUES ('entity', 'rdfs:isDefinedBy','http://www.w3.org/ns/prov-o#');
INSERT INTO table_metadata (TABLE_NAME, PREDICATE, OBJECT) 
    VALUES ('ntity', 'prov:category','starting-point');
INSERT INTO table_metadata (TABLE_NAME, PREDICATE, OBJECT) 
    VALUES ('entity', 'prov:component','entities-activities');
INSERT INTO table_metadata (TABLE_NAME, PREDICATE, OBJECT) 
    VALUES ('entity', 'prov:constraints','http://www.w3.org/TR/2013/REC-prov-constraints-20130430/#prov-dm-constraints-fig');
INSERT INTO table_metadata (TABLE_NAME, PREDICATE, OBJECT) 
    VALUES ('entity', 'prov:definition','An entity is a physical, digital, conceptual, or other kind of thing with some fixed aspects; entities may be real or imaginary. ');
INSERT INTO table_metadata (TABLE_NAME, PREDICATE, OBJECT) 
    VALUES ('entity', 'prov:dm','http://www.w3.org/TR/2013/REC-prov-dm-20130430/#term-entity');
INSERT INTO table_metadata (TABLE_NAME, PREDICATE, OBJECT) 
    VALUES ('entity', 'prov:n','http://www.w3.org/TR/2013/REC-prov-n-20130430/#expression-entity');

-- ======================================================================


CREATE TABLE IF NOT EXISTS Bundle (
    NAME TEXT PRIMARY KEY,
    GENERATED_AT_TIME DATE,
    INVALIDATED_AT_TIME DATE,
    AT_LOCATION TEXT,
    HAD_PRIMARY_SOURCE TEXT,
    ALTERNATE_OF TEXT,
    QUALIFIED_ATTRIBUTION TEXT,
    QUALIFIED_DERIVATION TEXT,
    QUALIFIED_GENERATION TEXT,
    QUALIFIED_INFLUENCE TEXT,
    QUALIFIED_INVALIDATION TEXT,
    QUALIFIED_PRIMARY_SOURCE TEXT,
    QUALIFIED_QUOTATION TEXT,
    QUALIFIED_REVISION TEXT,
    SPECIALIZATION_OF TEXT,
    HAS_VALUE TEXT,
    WAS_ATTRIBUTED_TO TEXT,
    WAS_DERIVED_FROM TEXT,
    WAS_GENERATED_BY TEXT,
    WAS_INFLUENCED_BY_ACTIVITY TEXT,
    WAS_INFLUENCED_BY_AGENT TEXT,
    WAS_INFLUENCED_BY_ENTITY TEXT,
    WAS_INVALIDATED_BY TEXT,
    WAS_QUOTED_FROM TEXT

);

INSERT INTO table_metadata (TABLE_NAME, PREDICATE, OBJECT) 
    VALUES ('bundle', 'rdfs:subClassOf', 'entity');

-- ======================================================================

CREATE TABLE IF NOT EXISTS Collection (
    HAD_MEMBER TEXT,
    NAME TEXT PRIMARY KEY,
    GENERATED_AT_TIME DATE,
    INVALIDATED_AT_TIME DATE,
    AT_LOCATION TEXT,
    HAD_PRIMARY_SOURCE TEXT,
    ALTERNATE_OF TEXT,
    QUALIFIED_ATTRIBUTION TEXT,
    QUALIFIED_DERIVATION TEXT,
    QUALIFIED_GENERATION TEXT,
    QUALIFIED_INFLUENCE TEXT,
    QUALIFIED_INVALIDATION TEXT,
    QUALIFIED_PRIMARY_SOURCE TEXT,
    QUALIFIED_QUOTATION TEXT,
    QUALIFIED_REVISION TEXT,
    SPECIALIZATION_OF TEXT,
    HAS_VALUE TEXT,
    WAS_ATTRIBUTED_TO TEXT,
    WAS_DERIVED_FROM TEXT,
    WAS_GENERATED_BY TEXT,
    WAS_INFLUENCED_BY_ACTIVITY TEXT,
    WAS_INFLUENCED_BY_AGENT TEXT,
    WAS_INFLUENCED_BY_ENTITY TEXT,
    WAS_INVALIDATED_BY TEXT,
    WAS_QUOTED_FROM TEXT

);

INSERT INTO table_metadata (TABLE_NAME, PREDICATE, OBJECT) 
    VALUES ('collection', 'rdfs:subClassOf', 'entity');


-- ======================================================================

CREATE TABLE IF NOT EXISTS empty_collection (
    HAD_MEMBER TEXT,
    NAME TEXT PRIMARY KEY,
    GENERATED_AT_TIME DATE,
    INVALIDATED_AT_TIME DATE,
    AT_LOCATION TEXT,
    HAD_PRIMARY_SOURCE TEXT,
    ALTERNATE_OF TEXT,
    QUALIFIED_ATTRIBUTION TEXT,
    QUALIFIED_DERIVATION TEXT,
    QUALIFIED_GENERATION TEXT,
    QUALIFIED_INFLUENCE TEXT,
    QUALIFIED_INVALIDATION TEXT,
    QUALIFIED_PRIMARY_SOURCE TEXT,
    QUALIFIED_QUOTATION TEXT,
    QUALIFIED_REVISION TEXT,
    SPECIALIZATION_OF TEXT,
    HAS_VALUE TEXT,
    WAS_ATTRIBUTED_TO TEXT,
    WAS_DERIVED_FROM TEXT,
    WAS_GENERATED_BY TEXT,
    WAS_INFLUENCED_BY_ACTIVITY TEXT,
    WAS_INFLUENCED_BY_AGENT TEXT,
    WAS_INFLUENCED_BY_ENTITY TEXT,
    WAS_INVALIDATED_BY TEXT,
    WAS_QUOTED_FROM TEXT

);

INSERT INTO table_metadata (TABLE_NAME, PREDICATE, OBJECT) 
    VALUES ('empty_collection', 'rdfs:subClassOf', 'collection');
INSERT INTO table_metadata (TABLE_NAME, PREDICATE, OBJECT) 
    VALUES ('empty_collection', 'rdfs:label','EmptyCollection');
INSERT INTO table_metadata (TABLE_NAME, PREDICATE, OBJECT) 
    VALUES ('empty_collection', 'rdfs:isDefinedBy','http://www.w3.org/ns/prov-o#');
INSERT INTO table_metadata (TABLE_NAME, PREDICATE, OBJECT) 
    VALUES ('empty_collection', 'prov:category','expanded');
INSERT INTO table_metadata (TABLE_NAME, PREDICATE, OBJECT) 
    VALUES ('empty_collection', 'prov:component','collections');
INSERT INTO table_metadata (TABLE_NAME, PREDICATE, OBJECT) 
    VALUES ('empty_collection', 'prov:definition','An empty collection is a collection without members');

INSERT INTO empty_collection (NAME) values ('EmptyCollection');

INSERT INTO row_metadata (TABLE_NAME, PRIMARY_KEY, PREDICATE, OBJECT, LANGUAGE)
    VALUES ('empty_collection', 'EmptyCollection', 'rdfs:isDefinedBy','http://www.w3.org/ns/prov-o#', 'en');;
INSERT INTO row_metadata (TABLE_NAME, PRIMARY_KEY, PREDICATE, OBJECT) 
    VALUES ('empty_collection', 'EmptyCollection', 'prov:category','expanded');
INSERT INTO row_metadata (TABLE_NAME, PRIMARY_KEY, PREDICATE, OBJECT) 
    VALUES ('empty_collection', 'EmptyCollection', 'prov:component','collections');
INSERT INTO row_metadata (TABLE_NAME, PRIMARY_KEY, PREDICATE, OBJECT) 
    VALUES ('empty_collection', 'EmptyCollection', 'prov:definition','An empty collection is a collection without members');

-- ======================================================================

CREATE TABLE IF NOT EXISTS plan (
    NAME TEXT PRIMARY KEY,
    GENERATED_AT_TIME DATE,
    INVALIDATED_AT_TIME DATE,
    AT_LOCATION TEXT,
    HAD_PRIMARY_SOURCE TEXT,
    ALTERNATE_OF TEXT,
    QUALIFIED_ATTRIBUTION TEXT,
    QUALIFIED_DERIVATION TEXT,
    QUALIFIED_GENERATION TEXT,
    QUALIFIED_INFLUENCE TEXT,
    QUALIFIED_INVALIDATION TEXT,
    QUALIFIED_PRIMARY_SOURCE TEXT,
    QUALIFIED_QUOTATION TEXT,
    QUALIFIED_REVISION TEXT,
    SPECIALIZATION_OF TEXT,
    HAS_VALUE TEXT,
    WAS_ATTRIBUTED_TO TEXT,
    WAS_DERIVED_FROM TEXT,
    WAS_GENERATED_BY TEXT,
    WAS_INFLUENCED_BY_ACTIVITY TEXT,
    WAS_INFLUENCED_BY_AGENT TEXT,
    WAS_INFLUENCED_BY_ENTITY TEXT,
    WAS_INVALIDATED_BY TEXT,
    WAS_QUOTED_FROM TEXT

);

INSERT INTO table_metadata (TABLE_NAME, PREDICATE, OBJECT) 
    VALUES ('plan', 'rdfs:subClassOf', 'entity');

-- ======================================================================

CREATE TABLE IF NOT EXISTS influence (
    NAME TEXT PRIMARY KEY
);

-- ======================================================================

CREATE TABLE IF NOT EXISTS activity_influence (
    NAME TEXT PRIMARY KEY
);

INSERT INTO table_metadata (TABLE_NAME, PREDICATE, OBJECT) 
    VALUES ('actvity_influence', 'rdfs:subClassOf', 'influence');


-- ======================================================================

CREATE TABLE IF NOT EXISTS Communication (
    NAME TEXT PRIMARY KEY
);

INSERT INTO table_metadata (TABLE_NAME, PREDICATE, OBJECT) 
    VALUES ('communication', 'rdfs:subClassOf', 'activity_influence');

-- ======================================================================

CREATE TABLE IF NOT EXISTS generation (
    NAME TEXT PRIMARY KEY
);

INSERT INTO table_metadata (TABLE_NAME, PREDICATE, OBJECT) 
    VALUES ('generation', 'rdfs:subClassOf', 'activity_influence');
INSERT INTO table_metadata (TABLE_NAME, PREDICATE, OBJECT) 
    VALUES ('generation', 'rdfs:subClassOf', 'instantaneous_event');

-- ======================================================================

CREATE TABLE IF NOT EXISTS invalidation (
    NAME TEXT PRIMARY KEY
);

INSERT INTO table_metadata (TABLE_NAME, PREDICATE, OBJECT) 
    VALUES ('invalidation', 'rdfs:subClassOf', 'activity_influence');
INSERT INTO table_metadata (TABLE_NAME, PREDICATE, OBJECT) 
    VALUES ('invalidation', 'rdfs:subClassOf', 'instantaneous_event');

-- ======================================================================

CREATE TABLE IF NOT EXISTS agent_influence (
    NAME TEXT PRIMARY KEY
);

INSERT INTO table_metadata (TABLE_NAME, PREDICATE, OBJECT) 
    VALUES ('agent_influence', 'rdfs:subClassOf', 'influence');

-- ======================================================================

CREATE TABLE IF NOT EXISTS association (
    NAME TEXT PRIMARY KEY,
    HAD_PLAN TEXT
);

INSERT INTO table_metadata (TABLE_NAME, PREDICATE, OBJECT) 
    VALUES ('association', 'rdfs:subClassOf', 'agent_influence');

-- ======================================================================

CREATE TABLE IF NOT EXISTS attribution (
    NAME TEXT PRIMARY KEY
);

INSERT INTO table_metadata (TABLE_NAME, PREDICATE, OBJECT) 
    VALUES ('attribution', 'rdfs:subClassOf', 'agent_influence');

-- ======================================================================

CREATE TABLE IF NOT EXISTS delegation (
    NAME TEXT PRIMARY KEY
);

INSERT INTO table_metadata (TABLE_NAME, PREDICATE, OBJECT) 
    VALUES ('delegation', 'rdfs:subClassOf', 'agent_influence');

-- ======================================================================

CREATE TABLE IF NOT EXISTS entity_influence (
    NAME TEXT PRIMARY KEY
);

INSERT INTO table_metadata (TABLE_NAME, PREDICATE, OBJECT) 
    VALUES ('entity_influence', 'rdfs:subClassOf', 'influence');

-- ======================================================================

CREATE TABLE IF NOT EXISTS derivation (
    NAME TEXT PRIMARY KEY
);

INSERT INTO table_metadata (TABLE_NAME, PREDICATE, OBJECT) 
    VALUES ('derivation', 'rdfs:subClassOf', 'prov:entity_influence');

-- ======================================================================

CREATE TABLE IF NOT EXISTS primary_source (
    NAME TEXT PRIMARY KEY
);

INSERT INTO table_metadata (TABLE_NAME, PREDICATE, OBJECT) 
    VALUES ('primary_source', 'rdfs:subClassOf', 'derivation');

-- ======================================================================

CREATE TABLE IF NOT EXISTS quotation (
    NAME TEXT PRIMARY KEY
);

INSERT INTO table_metadata (TABLE_NAME, PREDICATE, OBJECT) 
    VALUES ('quotation', 'rdfs:subClassOf', 'derivation');

-- ======================================================================

CREATE TABLE IF NOT EXISTS revision (
    NAME TEXT PRIMARY KEY
);

INSERT INTO table_metadata (TABLE_NAME, PREDICATE, OBJECT) 
    VALUES ('revision', 'rdfs:subClassOf', 'derivation');

-- ======================================================================

CREATE TABLE IF NOT EXISTS _end (
    NAME TEXT PRIMARY KEY
);

INSERT INTO table_metadata (TABLE_NAME, PREDICATE, OBJECT) 
    VALUES ('_end', 'rdfs:subClassOf', 'instantaneous_event');
INSERT INTO table_metadata (TABLE_NAME, PREDICATE, OBJECT) 
    VALUES ('_end', 'rdfs:subClassOf', 'prov:entity_influence');

-- ======================================================================

CREATE TABLE IF NOT EXISTS start (
    NAME TEXT PRIMARY KEY
);

INSERT INTO table_metadata (TABLE_NAME, PREDICATE, OBJECT) 
    VALUES ('start', 'rdfs:subClassOf', 'instantaneous_event');
INSERT INTO table_metadata (TABLE_NAME, PREDICATE, OBJECT) 
    VALUES ('start', 'rdfs:subClassOf', 'prov:entity_influence');

-- ======================================================================

CREATE TABLE IF NOT EXISTS usage (
    NAME TEXT PRIMARY KEY
);

INSERT INTO table_metadata (TABLE_NAME, PREDICATE, OBJECT) 
    VALUES ('usage', 'rdfs:subClassOf', 'instantaneous_event');
INSERT INTO table_metadata (TABLE_NAME, PREDICATE, OBJECT) 
    VALUES ('usage', 'rdfs:subClassOf', 'prov:entity_influence');


-- ======================================================================

CREATE TABLE IF NOT EXISTS instantaneous_event (
    NAME TEXT PRIMARY KEY
);

-- ======================================================================

CREATE TABLE IF NOT EXISTS role (
    NAME TEXT PRIMARY KEY
);

-- ======================================================================

CREATE TABLE IF NOT EXISTS location (
    NAME TEXT PRIMARY KEY
);


-- ======================================================================
-- Reifications

CREATE TABLE IF NOT EXISTS had_member_entity (
    COLLECTION TEXT,
    MEMBER TEXT,
    PRIMARY KEY (COLLECTION, MEMBER),
    FOREIGN KEY (COLLECTION)
        REFERENCES collection(NAME),
    FOREIGN KEY (MEMBER)
        REFERENCES entity(NAME)
);

CREATE TABLE IF NOT EXISTS had_member_bundle (
    COLLECTION TEXT,
    MEMBER TEXT,
    PRIMARY KEY (COLLECTION, MEMBER),
    FOREIGN KEY (COLLECTION)
        REFERENCES collection(NAME),
    FOREIGN KEY (MEMBER)
        REFERENCES bundle(NAME)
);

CREATE TABLE IF NOT EXISTS had_member_collection (
    COLLECTION TEXT,
    MEMBER TEXT,
    PRIMARY KEY (COLLECTION, MEMBER),
    FOREIGN KEY (COLLECTION)
        REFERENCES collection(NAME),
    FOREIGN KEY (MEMBER)
        REFERENCES collection(NAME)
);

CREATE TABLE IF NOT EXISTS had_member_empty_collection (
    COLLECTION TEXT,
    MEMBER TEXT,
    PRIMARY KEY (COLLECTION, MEMBER),
    FOREIGN KEY (COLLECTION)
        REFERENCES collection(NAME),
    FOREIGN KEY (MEMBER)
        REFERENCES empty_collection(NAME)
);

CREATE TABLE IF NOT EXISTS had_member_plan (
    COLLECTION TEXT,
    MEMBER TEXT,
    PRIMARY KEY (COLLECTION, MEMBER),
    FOREIGN KEY (COLLECTION)
        REFERENCES collection(NAME),
    FOREIGN KEY (MEMBER)
        REFERENCES plan(NAME)
);

CREATE TABLE IF NOT EXISTS used_entity (
    ACTIVITY TEXT,
    SOURCE TEXT,
    PRIMARY KEY (ACTIVITY, SOURCE),
    FOREIGN KEY (ACTIVITY)
        REFERENCES activity(NAME),
    FOREIGN KEY (SOURCE)
        REFERENCES entity(NAME)
);

CREATE TABLE IF NOT EXISTS used_bundle (
    ACTIVITY TEXT,
    SOURCE TEXT,
    PRIMARY KEY (ACTIVITY, SOURCE),
    FOREIGN KEY (ACTIVITY)
        REFERENCES activity(NAME),
    FOREIGN KEY (SOURCE)
        REFERENCES bundle(NAME)
);

CREATE TABLE IF NOT EXISTS used_collection (
    ACTIVITY TEXT,
    SOURCE TEXT,
    PRIMARY KEY (ACTIVITY, SOURCE),
    FOREIGN KEY (ACTIVITY)
        REFERENCES activity(NAME),
    FOREIGN KEY (SOURCE)
        REFERENCES collection(NAME)
);

CREATE TABLE IF NOT EXISTS used_empty_collection (
    ACTIVITY TEXT,
    SOURCE TEXT,
    PRIMARY KEY (ACTIVITY, SOURCE),
    FOREIGN KEY (ACTIVITY)
        REFERENCES activity(NAME),
    FOREIGN KEY (SOURCE)
        REFERENCES empty_collection(NAME)
);

CREATE TABLE IF NOT EXISTS used_plan (
    ACTIVITY TEXT,
    SOURCE TEXT,
    PRIMARY KEY (ACTIVITY, SOURCE),
    FOREIGN KEY (ACTIVITY)
        REFERENCES activity(NAME),
    FOREIGN KEY (SOURCE)
        REFERENCES entity(NAME)
);

ALTER TABLE activity
    ADD FOREIGN KEY (AT_LOCATION) 
    REFERENCES Location(NAME),
    ADD FOREIGN KEY (GENERATED) 
    REFERENCES entity(NAME),
    ADD FOREIGN KEY (INVALIDATED)
    REFERENCES entity(NAME),
    ADD FOREIGN KEY (QUALFIED_ASSOCIATION)
    REFERENCES Association(NAME),
    ADD FOREIGN KEY (QUALFIED_COMMUNICATION)
    REFERENCES Communication(NAME),
    ADD FOREIGN KEY (QUALFIED_END)
    REFERENCES _End(NAME),
    ADD FOREIGN KEY (QUALFIED_INFLUENCE)
    REFERENCES Influence(NAME),
    ADD FOREIGN KEY (QUALFIED_START)
    REFERENCES Start(NAME),
    ADD FOREIGN KEY (QUALFIED_USAGE)
    REFERENCES Usage(NAME),
    ADD FOREIGN KEY (WAS_ASSOCIATED_WITH)
    REFERENCES agent(NAME),
    ADD FOREIGN KEY (WAS_ENDED_BY)
    REFERENCES entity(NAME),
    ADD FOREIGN KEY (WAS_GENERATED_BY)
    REFERENCES entity(NAME),
    ADD FOREIGN KEY (WAS_INFLUENCED_BY_ACTIVITY)
    REFERENCES activity(NAME),
    ADD FOREIGN KEY (WAS_INFLUENCED_BY_AGENT)
    REFERENCES agent(NAME),
    ADD FOREIGN KEY (WAS_INFLUENCED_BY_ENTITY)
    REFERENCES entity(NAME),
    ADD FOREIGN KEY (WAS_INFORMED_BY)
    REFERENCES activity(NAME),
    ADD FOREIGN KEY (WAS_STARTED_BY)
    REFERENCES entity(NAME)
;

ALTER TABLE agent
    ADD FOREIGN KEY (AT_LOCATION) 
    REFERENCES Location(NAME),
    ADD FOREIGN KEY (ACTED_ON_BEHALF_OF) 
    REFERENCES agent(NAME),
    ADD FOREIGN KEY (WAS_INFLUENCED_BY_ACTIVITY)
    REFERENCES activity(NAME),
    ADD FOREIGN KEY (WAS_INFLUENCED_BY_AGENT)
    REFERENCES agent(NAME),
    ADD FOREIGN KEY (WAS_INFLUENCED_BY_ENTITY)
    REFERENCES entity(NAME),
    ADD FOREIGN KEY (QUALIFIED_DELEGATION)
    REFERENCES Delegation(NAME),
    ADD FOREIGN KEY (QUALIFIED_INFLUENCE)
    REFERENCES Influence(NAME)
;

ALTER TABLE organization
    ADD FOREIGN KEY (AT_LOCATION) 
    REFERENCES Location(NAME),
    ADD FOREIGN KEY (ACTED_ON_BEHALF_OF) 
    REFERENCES agent(NAME),
    ADD FOREIGN KEY (WAS_INFLUENCED_BY_ACTIVITY)
    REFERENCES activity(NAME),
    ADD FOREIGN KEY (WAS_INFLUENCED_BY_AGENT)
    REFERENCES agent(NAME),
    ADD FOREIGN KEY (WAS_INFLUENCED_BY_ENTITY)
    REFERENCES entity(NAME),
    ADD FOREIGN KEY (QUALIFIED_DELEGATION)
    REFERENCES Delegation(NAME),
    ADD FOREIGN KEY (QUALIFIED_INFLUENCE)
    REFERENCES Influence(NAME)
;

ALTER TABLE person
    ADD FOREIGN KEY (AT_LOCATION) 
    REFERENCES Location(NAME),
    ADD FOREIGN KEY (ACTED_ON_BEHALF_OF) 
    REFERENCES agent(NAME),
    ADD FOREIGN KEY (WAS_INFLUENCED_BY_ACTIVITY)
    REFERENCES activity(NAME),
    ADD FOREIGN KEY (WAS_INFLUENCED_BY_AGENT)
    REFERENCES agent(NAME),
    ADD FOREIGN KEY (WAS_INFLUENCED_BY_ENTITY)
    REFERENCES entity(NAME),
    ADD FOREIGN KEY (QUALIFIED_DELEGATION)
    REFERENCES Delegation(NAME),
    ADD FOREIGN KEY (QUALIFIED_INFLUENCE)
    REFERENCES Influence(NAME)
;

ALTER TABLE software_agent
    ADD FOREIGN KEY (AT_LOCATION) 
    REFERENCES Location(NAME),
    ADD FOREIGN KEY (ACTED_ON_BEHALF_OF) 
    REFERENCES agent(NAME),
    ADD FOREIGN KEY (WAS_INFLUENCED_BY_ACTIVITY)
    REFERENCES activity(NAME),
    ADD FOREIGN KEY (WAS_INFLUENCED_BY_AGENT)
    REFERENCES agent(NAME),
    ADD FOREIGN KEY (WAS_INFLUENCED_BY_ENTITY)
    REFERENCES entity(NAME),
    ADD FOREIGN KEY (QUALIFIED_DELEGATION)
    REFERENCES Delegation(NAME),
    ADD FOREIGN KEY (QUALIFIED_INFLUENCE)
    REFERENCES Influence(NAME)
;

ALTER TABLE entity
    ADD FOREIGN KEY (AT_LOCATION) 
    REFERENCES location(NAME),
    ADD FOREIGN KEY (ALTERNATE_OF)
    REFERENCES activity(NAME),
    ADD FOREIGN KEY (WAS_INFLUENCED_BY_ACTIVITY)
    REFERENCES activity(NAME),
    ADD FOREIGN KEY (WAS_INFLUENCED_BY_AGENT)
    REFERENCES agent(NAME),
    ADD FOREIGN KEY (WAS_INFLUENCED_BY_ENTITY)
    REFERENCES entity(NAME),
    ADD FOREIGN KEY (WAS_INVALIDATED_BY)
    REFERENCES activity(NAME),
    ADD FOREIGN KEY (QUALIFIED_ATTRIBUTION)
    REFERENCES attribution(NAME),
    ADD FOREIGN KEY (QUALIFIED_DERIVATION)
    REFERENCES derivation(NAME),
    ADD FOREIGN KEY (QUALIFIED_GENERATION)
    REFERENCES generation(NAME),
    ADD FOREIGN KEY (QUALIFIED_INFLUENCE)
    REFERENCES influence(NAME),
    ADD FOREIGN KEY (QUALIFIED_INVALIDATION)
    REFERENCES invalidation(NAME),
    ADD FOREIGN KEY (QUALIFIED_PRIMARY_SOURCE)
    REFERENCES primary_source(NAME),
    ADD FOREIGN KEY (QUALIFIED_QUOTATION)
    REFERENCES quotation(NAME),
    ADD FOREIGN KEY (QUALIFIED_REVISION)
    REFERENCES revision(NAME),
    ADD FOREIGN KEY (SPECIALIZATION_OF)
    REFERENCES entity(NAME),
    ADD FOREIGN KEY (WAS_ATTRIBUTED_TO)
    REFERENCES agent(NAME),
    ADD FOREIGN KEY (WAS_DERIVED_FROM)
    REFERENCES entity(NAME),
    ADD FOREIGN KEY (WAS_GENERATED_BY)
    REFERENCES entity(NAME),
    ADD FOREIGN KEY (WAS_QUOTED_FROM)
    REFERENCES entity(NAME)
;

ALTER TABLE bundle
    ADD FOREIGN KEY (AT_LOCATION) 
    REFERENCES location(NAME),
    ADD FOREIGN KEY (ALTERNATE_OF)
    REFERENCES activity(NAME),
    ADD FOREIGN KEY (WAS_INFLUENCED_BY_ACTIVITY)
    REFERENCES activity(NAME),
    ADD FOREIGN KEY (WAS_INFLUENCED_BY_AGENT)
    REFERENCES agent(NAME),
    ADD FOREIGN KEY (WAS_INFLUENCED_BY_ENTITY)
    REFERENCES entity(NAME),
    ADD FOREIGN KEY (WAS_INVALIDATED_BY)
    REFERENCES activity(NAME),
    ADD FOREIGN KEY (QUALIFIED_ATTRIBUTION)
    REFERENCES attribution(NAME),
    ADD FOREIGN KEY (QUALIFIED_DERIVATION)
    REFERENCES derivation(NAME),
    ADD FOREIGN KEY (QUALIFIED_GENERATION)
    REFERENCES generation(NAME),
    ADD FOREIGN KEY (QUALIFIED_INFLUENCE)
    REFERENCES influence(NAME),
    ADD FOREIGN KEY (QUALIFIED_INVALIDATION)
    REFERENCES invalidation(NAME),
    ADD FOREIGN KEY (QUALIFIED_PRIMARY_SOURCE)
    REFERENCES primary_source(NAME),
    ADD FOREIGN KEY (QUALIFIED_QUOTATION)
    REFERENCES quotation(NAME),
    ADD FOREIGN KEY (QUALIFIED_REVISION)
    REFERENCES revision(NAME),
    ADD FOREIGN KEY (SPECIALIZATION_OF)
    REFERENCES entity(NAME),
    ADD FOREIGN KEY (WAS_ATTRIBUTED_TO)
    REFERENCES agent(NAME),
    ADD FOREIGN KEY (WAS_DERIVED_FROM)
    REFERENCES entity(NAME),
    ADD FOREIGN KEY (WAS_GENERATED_BY)
    REFERENCES entity(NAME),
    ADD FOREIGN KEY (WAS_QUOTED_FROM)
    REFERENCES entity(NAME)
;

ALTER TABLE collection
    ADD FOREIGN KEY (AT_LOCATION) 
    REFERENCES location(NAME),
    ADD FOREIGN KEY (ALTERNATE_OF)
    REFERENCES activity(NAME),
    ADD FOREIGN KEY (WAS_INFLUENCED_BY_ACTIVITY)
    REFERENCES activity(NAME),
    ADD FOREIGN KEY (WAS_INFLUENCED_BY_AGENT)
    REFERENCES agent(NAME),
    ADD FOREIGN KEY (WAS_INFLUENCED_BY_ENTITY)
    REFERENCES entity(NAME),
    ADD FOREIGN KEY (WAS_INVALIDATED_BY)
    REFERENCES activity(NAME),
    ADD FOREIGN KEY (QUALIFIED_ATTRIBUTION)
    REFERENCES attribution(NAME),
    ADD FOREIGN KEY (QUALIFIED_DERIVATION)
    REFERENCES derivation(NAME),
    ADD FOREIGN KEY (QUALIFIED_GENERATION)
    REFERENCES generation(NAME),
    ADD FOREIGN KEY (QUALIFIED_INFLUENCE)
    REFERENCES influence(NAME),
    ADD FOREIGN KEY (QUALIFIED_INVALIDATION)
    REFERENCES invalidation(NAME),
    ADD FOREIGN KEY (QUALIFIED_PRIMARY_SOURCE)
    REFERENCES primary_source(NAME),
    ADD FOREIGN KEY (QUALIFIED_QUOTATION)
    REFERENCES quotation(NAME),
    ADD FOREIGN KEY (QUALIFIED_REVISION)
    REFERENCES revision(NAME),
    ADD FOREIGN KEY (SPECIALIZATION_OF)
    REFERENCES entity(NAME),
    ADD FOREIGN KEY (WAS_ATTRIBUTED_TO)
    REFERENCES agent(NAME),
    ADD FOREIGN KEY (WAS_DERIVED_FROM)
    REFERENCES entity(NAME),
    ADD FOREIGN KEY (WAS_GENERATED_BY)
    REFERENCES entity(NAME),
    ADD FOREIGN KEY (WAS_QUOTED_FROM)
    REFERENCES entity(NAME)
;

ALTER TABLE empty_collection
    ADD FOREIGN KEY (AT_LOCATION) 
    REFERENCES location(NAME),
    ADD FOREIGN KEY (ALTERNATE_OF)
    REFERENCES activity(NAME),
    ADD FOREIGN KEY (WAS_INFLUENCED_BY_ACTIVITY)
    REFERENCES activity(NAME),
    ADD FOREIGN KEY (WAS_INFLUENCED_BY_AGENT)
    REFERENCES agent(NAME),
    ADD FOREIGN KEY (WAS_INFLUENCED_BY_ENTITY)
    REFERENCES entity(NAME),
    ADD FOREIGN KEY (WAS_INVALIDATED_BY)
    REFERENCES activity(NAME),
    ADD FOREIGN KEY (QUALIFIED_ATTRIBUTION)
    REFERENCES attribution(NAME),
    ADD FOREIGN KEY (QUALIFIED_DERIVATION)
    REFERENCES derivation(NAME),
    ADD FOREIGN KEY (QUALIFIED_GENERATION)
    REFERENCES generation(NAME),
    ADD FOREIGN KEY (QUALIFIED_INFLUENCE)
    REFERENCES influence(NAME),
    ADD FOREIGN KEY (QUALIFIED_INVALIDATION)
    REFERENCES invalidation(NAME),
    ADD FOREIGN KEY (QUALIFIED_PRIMARY_SOURCE)
    REFERENCES primary_source(NAME),
    ADD FOREIGN KEY (QUALIFIED_QUOTATION)
    REFERENCES quotation(NAME),
    ADD FOREIGN KEY (QUALIFIED_REVISION)
    REFERENCES revision(NAME),
    ADD FOREIGN KEY (SPECIALIZATION_OF)
    REFERENCES entity(NAME),
    ADD FOREIGN KEY (WAS_ATTRIBUTED_TO)
    REFERENCES agent(NAME),
    ADD FOREIGN KEY (WAS_DERIVED_FROM)
    REFERENCES entity(NAME),
    ADD FOREIGN KEY (WAS_GENERATED_BY)
    REFERENCES entity(NAME),
    ADD FOREIGN KEY (WAS_QUOTED_FROM)
    REFERENCES entity(NAME)
;

ALTER TABLE plan
    ADD FOREIGN KEY (AT_LOCATION) 
    REFERENCES location(NAME),
    ADD FOREIGN KEY (ALTERNATE_OF)
    REFERENCES activity(NAME),
    ADD FOREIGN KEY (WAS_INFLUENCED_BY_ACTIVITY)
    REFERENCES activity(NAME),
    ADD FOREIGN KEY (WAS_INFLUENCED_BY_AGENT)
    REFERENCES agent(NAME),
    ADD FOREIGN KEY (WAS_INFLUENCED_BY_ENTITY)
    REFERENCES entity(NAME),
    ADD FOREIGN KEY (WAS_INVALIDATED_BY)
    REFERENCES activity(NAME),
    ADD FOREIGN KEY (QUALIFIED_ATTRIBUTION)
    REFERENCES attribution(NAME),
    ADD FOREIGN KEY (QUALIFIED_DERIVATION)
    REFERENCES derivation(NAME),
    ADD FOREIGN KEY (QUALIFIED_GENERATION)
    REFERENCES generation(NAME),
    ADD FOREIGN KEY (QUALIFIED_INFLUENCE)
    REFERENCES influence(NAME),
    ADD FOREIGN KEY (QUALIFIED_INVALIDATION)
    REFERENCES invalidation(NAME),
    ADD FOREIGN KEY (QUALIFIED_PRIMARY_SOURCE)
    REFERENCES primary_source(NAME),
    ADD FOREIGN KEY (QUALIFIED_QUOTATION)
    REFERENCES quotation(NAME),
    ADD FOREIGN KEY (QUALIFIED_REVISION)
    REFERENCES revision(NAME),
    ADD FOREIGN KEY (SPECIALIZATION_OF)
    REFERENCES entity(NAME),
    ADD FOREIGN KEY (WAS_ATTRIBUTED_TO)
    REFERENCES agent(NAME),
    ADD FOREIGN KEY (WAS_DERIVED_FROM)
    REFERENCES entity(NAME),
    ADD FOREIGN KEY (WAS_GENERATED_BY)
    REFERENCES entity(NAME),
    ADD FOREIGN KEY (WAS_QUOTED_FROM)
    REFERENCES entity(NAME)
;

ALTER TABLE association
    ADD FOREIGN KEY (HAD_PLAN)
    REFERENCES plan(NAME)
;
