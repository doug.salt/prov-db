#!/usr/bin/perl

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

# hune 2019

our $VERSION = "0.1";

=encoding UTF-8

=head1 NAME

A small script to do recursive pre-processing of include files on a sql file.

=head1 SYNOPSIS

./preprocess.pl base_file.sql > sql_with_files_included.sql

=cut

&preprocess($ARGV[0]);

sub preprocess {
    open my $INPUT, "<" . $_[0]
        || die "$0: Unable to open: $_[0]: $!";
    while (my $line = <$INPUT>) {
        if ($line =~ /\#include\<(.*)\>/) {
            &preprocess($1);
        } else {
            print $line;
        }
    }
    close $INPUT;
}

=head1 AUTHOR

Written by Doug Salt.

=head1 REPORTING BUGS

Please send all bug reports to info@hutton.ac.uk.

=head1 COPYRIGHT

Copyright © 2020  The James Hutton Institute.  License GPLv3+: GNU
GPL version 3 or later L<http://gnu.org/licenses/gpl.html>.
This is free software: you are free  to  change  and  redistribute  it.
There is NO WARRANTY, to the extent permitted by law.

=head1 SEE ALSO


