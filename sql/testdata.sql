insert into agent (name) values ('Doug Salt');
insert into agent (name) values ('Gary Polhill');
insert into agent (name) values ('Jiaqi Ge');
insert into agent (name) values ('Tony Craig');

insert into entity (name) values ('document_a');
insert into entity (name) values ('document_b');
insert into entity (name) values ('document_c');

insert into activity (name, started_at_time, ended_at_time) 
    values ('writing_document_a','2018-10-10 10:10:10','2018-10-10 11:10:10');
insert into activity (name, started_at_time, ended_at_time)  
    values ('updating_document_a','2018-10-11 10:10:10','2018-10-11 11:10:10');
insert into activity  (name, started_at_time, ended_at_time)  
    values ('reviewing_document_a','2018-10-12 10:10:10','2018-10-12 11:10:10');

insert into activity (name, started_at_time, ended_at_time) 
    values ('writing_document_b','2018-10-10 10:10:10','2018-10-10 11:10:10');
insert into activity (name, started_at_time, ended_at_time)  
    values ('updating_document_b','2018-10-11 10:10:10','2018-10-11 11:10:10');
insert into activity (name, started_at_time, ended_at_time)  
    values ('reviewing_document_b','2018-10-12 10:10:10','2018-10-12 11:10:10');

insert into activity (name, started_at_time, ended_at_time) 
    values ('writing_document_c','2018-10-10 10:10:10','2018-10-10 11:10:10');
insert into activity (name, started_at_time, ended_at_time)  
    values ('updating_document_c','2018-10-11 10:10:10','2018-10-11 11:10:10');
insert into activity (name, started_at_time, ended_at_time)  
    values ('reviewing_document_c','2018-10-12 10:10:10','2018-10-12 11:10:10');

insert into location (name) values ('Aberdeen');
insert into location (name) values ('Dundee');
