There are a lot of include files in this directory. These are of the form `*columns` and `*constraints` This is because the work of configuring the database is incomplete. However, use this directory then you would do something like

```bash
./preprocess.pl skeleton.origin > provo.sql

```

The contents of this directory are:

+ README.md - this file
+ activity.columns - this is a sql include file
+ activity.constraints - this is a sql include file
+ agent.columns - this is a sql include file
+ agent.constraints - this is a sql include file
+ derivation_influence.columns - this is a sql include file
+ drop-all.sql - this completely empties te data base.
+ entity.columns - this is a sql include file
+ entity.constraints - this is a sql include file
+ entity_influence.columns - this is a sql include file
+ influence.columns - this is a sql include file
+ influence.constraints - this is a sql include file
+ instantaneous_event.columns - this is a sql include file
+ instantaneous_events.constraints - this is a sql include file
+ post.sh - this tests the post command. I will add a few more of these at some pont.
+ preprocess.pl - this is a small perl program that takes the sql include files and add them to a relevant file. This is because I could find no standard way of having sql libraries. If someobody knows a more standard and better way of doing this, then please contact me at dougdotsaltathuttondotacdotuk
+ provo.skeleton - this the definition of database, using my  '-- #include<' include file '>' directive.
+ provo.sql - this is the output from `preprocess.pl`
+ testdata.sql - an attempt to start populating the database with some data for the purposes of testing.
